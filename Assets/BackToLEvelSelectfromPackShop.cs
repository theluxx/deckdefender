﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class BackToLEvelSelectfromPackShop : MonoBehaviour {

    public ThePackShopGems ThePack;
    public GetCardsFromPacks cardsFromPacks;
    public SpawnPacks SpawnPacks;
    public GameObject GetText;

     void Awake()
    {
        
    }

    // Use this for initialization
    void Start ()
    {
        ThePack = GameObject.Find("Gems").GetComponent<ThePackShopGems>();
        cardsFromPacks = GameObject.FindGameObjectWithTag("Packs").GetComponent<GetCardsFromPacks>();
        SpawnPacks = GameObject.Find("Content").GetComponent<SpawnPacks>();
        GetText.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Back()
    {
        if (SpawnPacks.Packs.Count <= 0)
        {
            SceneManager.LoadScene(1);
            PlayerPrefs.SetInt("Gems", ThePack.GemsPackShop);
            PlayerPrefs.SetInt("ResearchPoints", cardsFromPacks.ResearchPoints);
            DeckUtils.WriteDeck(cardsFromPacks.GetCardsOut, "PlayerCardPacks");
        }
        else
        {
            StartCoroutine(showMessage());
        }
    }

    IEnumerator showMessage()
    {
        GetText.SetActive(true);
        yield return new WaitForSeconds(3);
        GetText.SetActive(false);
    }

}
