﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragForCardPack : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Transform parentToReturnToo = null;

    public List<GameObject> CardsinPack = new List<GameObject>();

    public PersistentCardDatabase pcd;

   

    public OtherCardTypes otherCard;

    public GemsForWorkShop GemsForWork;

    int index = 0;

    public void OnBeginDrag(PointerEventData eventData)
    {
        parentToReturnToo = this.transform.parent;
        this.transform.SetParent(this.transform.parent.parent.parent);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(parentToReturnToo);
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    void Awake()
    {
        pcd = GameObject.Find("PersistentCardDatabase").GetComponent<PersistentCardDatabase>();
        otherCard = GameObject.Find("OtherCardTypes").GetComponent<OtherCardTypes>();
        GemsForWork = GameObject.Find("Gems").GetComponent<GemsForWorkShop>();


        for (int s = 0; s < 5; s++)
        {
            int stes = Random.Range(0, pcd.cardObjects.Count);
            CardsinPack.Add(pcd.cardObjects[stes]);

        }


        while (index < CardsinPack.Count - 1)
        {
            if (CardsinPack[index] == CardsinPack[index + 1])
            {
                CardsinPack.RemoveAt(index);
                CardsinPack.Add(pcd.cardObjects[Random.Range(0, pcd.cardObjects.Count)]);
            }
            else
            {
                index++;
            }
        }
    






    }

        
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
