﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPacks : MonoBehaviour
{
    public PersistentCardDatabase pcd;
    public ThePackShopGems PackShopGems;
    public GameObject CardPack;
    public GameObject PackCollection;

    public List<GameObject> Packs = new List<GameObject>();
  

    public int CostofPack = 1000;

    // Use this for initialization
    void Start()
    {
        pcd = GameObject.Find("PersistentCardDatabase").GetComponent<PersistentCardDatabase>();
        PackShopGems = GameObject.Find("Gems").GetComponent<ThePackShopGems>();
       
    }

    // Update is called once per frame
    void Update()
    {
        for (var i = Packs.Count - 1; i > -1; i--)
        {
            if (Packs[i] == null)
            {
                Packs.RemoveAt(i);
            }else
            {
                return;
            }
        }


    }

    public void BuyMyPackCard()
    {
        if (PackShopGems.GemsPackShop > CostofPack)
        {
            PackShopGems.GemsPackShop -= CostofPack;
            GameObject go = Instantiate(CardPack) as GameObject;
            go.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
            go.transform.SetParent(PackCollection.transform);
            Packs.Add(go.transform.GetChild(0).gameObject);
        }
    }
}
