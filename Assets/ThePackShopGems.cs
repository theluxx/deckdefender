﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ThePackShopGems : MonoBehaviour {

    public int GemsPackShop;
    public TextMeshProUGUI textPackShop;

    // Use this for initialization
    void Start ()
    {
        GemsPackShop = PlayerPrefs.GetInt("Gems", 0);
       

    }
	
	// Update is called once per frame
	void Update ()
    {
        textPackShop.text = GemsPackShop.ToString();

    }
}
