﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelPanel : MonoBehaviour {

    [SerializeField]
    private TowerMenu towerMenu;

    
    public void CloseAndReset()
    {
        towerMenu.CloseAndReset();
        this.gameObject.SetActive(false);
    }

}
