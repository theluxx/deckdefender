﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    public MissileLauncher _launcher;
    public GameObject _target;
    private float _timeToHit = 0.25f;
    // Use this for initialization
    void Start()
    {
		
    }
	
    // Update is called once per frame
    void Update()
    {
        if (_target.activeInHierarchy && _target != null)
        {
            transform.LookAt(_target.transform.position);
            transform.position = new Vector3(Mathf.Lerp(transform.position.x, _target.transform.position.x, _timeToHit),
                Mathf.Lerp(transform.position.y, _target.transform.position.y, _timeToHit),
                Mathf.Lerp(transform.position.z, _target.transform.position.z, _timeToHit));
        }
        else
        {
            gameObject.SetActive(false);
            _timeToHit = 0.25f;
        }
    }

    void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject == _target)
        {
            _launcher.Hit();
        }
    }

}
