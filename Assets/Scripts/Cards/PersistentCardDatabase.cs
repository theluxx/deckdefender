﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentCardDatabase : MonoBehaviour {

    [SerializeField]
    public CardList cardList;

    [SerializeField]
    public CardList defaultDeck;
   

    public List<GameObject> myCards = new List<GameObject>();

    public List<GameObject> cardObjects = new List<GameObject>();


    public List<string> cardIDs = new List<string>();

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        foreach (GameObject card in cardList.cardList)
        {

            cardIDs.Add(card.GetComponent<Card>().cardAsset.GetCardID());
            cardObjects.Add(card);

        }

       

    }

    public void Update()
    {

    }


}


