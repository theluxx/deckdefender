﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairTowers : CardAbility {



    [SerializeField]
    private Modifier modifier;

    [SerializeField]
    private TowerMenu towerMenu;


    private enum Modifier
    {
        Small,
        Medium,
        Large

    }


    public override void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            UseAction();
        }
    }


    public override void Start()
    {
        if(FindObjectOfType<Hand>() != null)
        {
            towerMenu = FindObjectOfType<Hand>().towerMenu;
        }
        
    }


    public override void UseAction()
    {
        TowerBase[] towers = FindObjectsOfType<TowerBase>();

        foreach(TowerBase tower in towers)
        {
            float amount = (tower.ReturnMaxHp() * StatMod());
            tower.HealTower((int)amount);
        }

        if(towerMenu.gameObject.activeInHierarchy)
        {
            if(towerMenu.tower != null)
            {
                towerMenu.SetCurrentTowerTexts();
            }
            
        }
        




    }



    public float StatMod()
    {
        if (modifier == Modifier.Small)
        {
            return .3f;
        }
        else if (modifier == Modifier.Medium)
        {
            return .6f;
        }
        else
        {
            return .8f;
        }

    }


}
