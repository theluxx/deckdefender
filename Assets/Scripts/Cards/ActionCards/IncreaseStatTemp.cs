﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseStatTemp : CardAbility {
    [SerializeField]
    protected int towers;

    [SerializeField]
    protected float duration;

    [SerializeField]
    protected float mod;






    [SerializeField]
    protected TowerMenu towerMenu;

    


    protected enum Modifier
    {
        Small,
        Medium,
        Large

    }

    protected enum Stat
    {
        Attack,
        Range,
        Speed
    }



    [SerializeField]
    protected Modifier modifier;

    [SerializeField]
    protected Stat stat;


    // Use this for initialization
    public override void Start()
    {
        if (FindObjectOfType<Hand>() != null)
        {
            towerMenu = FindObjectOfType<Hand>().towerMenu;
        }
        mod = StatMod();
    }

    // Update is called once per frame
    void Update()
    {


    }


    public override void UseAction()
    {

        TowerBase[] towers = FindObjectsOfType<TowerBase>();
        List<TowerBase> towerList = new List<TowerBase>();

        if (towers.Length == 0)
        {
            return;
        }


        foreach(TowerBase tower in towers)
        {
            towerList.Add(tower);
            if(stat == Stat.Attack)
            {
                tower.ActionCardModAtk(mod, true);
            }
            if (stat == Stat.Range)
            {
                tower.ActionCardModRange(mod, true);
            }
            if (stat == Stat.Speed)
            {
                tower.ActionCardModSpeed(mod, true);
            }



            if (towerMenu.gameObject.activeInHierarchy)
            {
                if(towerMenu.tower != null)
                {
                    towerMenu.SetCurrentTowerTexts();
                }
                
            }


            StartCoroutine(ResetStats(towerList));

        }





    }







    public virtual float StatMod()
    {
        if (modifier == Modifier.Small)
        {
            return 1.1f;
        }
        else if (modifier == Modifier.Medium)
        {
            return 1.2f;
        }
        else
        {
            return 1.3f;
        }

    }


    public IEnumerator ResetStats(List<TowerBase> towerList)
    {
        yield return new WaitForSeconds(duration);
        foreach(TowerBase tower in towerList)
        {
            if (stat == Stat.Attack)
            {
                tower.ActionCardModAtk(mod, false);
            }
            if (stat == Stat.Range)
            {
                tower.ActionCardModRange(mod, false);
            }
            if (stat == Stat.Speed)
            {
                tower.ActionCardModSpeed(mod, false);
            }



        }
        if(towerMenu.gameObject.activeInHierarchy)
        {
            if(towerMenu.tower != null)
            {
                towerMenu.SetCurrentTowerTexts();
            }
            
        }
        towerList.Clear();



    }



}
