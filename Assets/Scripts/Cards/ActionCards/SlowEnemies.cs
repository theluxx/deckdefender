﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;  

public class SlowEnemies : CardAbility {

    [SerializeField]
    protected int towers;

    [SerializeField]
    protected float duration;

    [SerializeField]
    protected float mod;

    [SerializeField]
    protected TowerMenu towerMenu;

    protected enum Modifier
    {
        Small,
        Medium,
        Large

    }


    [SerializeField]
    protected Modifier modifier;


    // Use this for initialization
    public override void Start()
    {
        if (FindObjectOfType<Hand>() != null)
        {
            towerMenu = FindObjectOfType<Hand>().towerMenu;
        }
        mod = StatMod();
    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.B))
        {
            UseAction();
        }
    }





    public override void UseAction()
    {

        EnemyScript[] enemies = FindObjectsOfType<EnemyScript>();
        List<NavMeshAgent> agents = new List<NavMeshAgent>();
        if (enemies.Length == 0)
        {
            return;
        }


        foreach(EnemyScript enemy in enemies)
        {
            NavMeshAgent navMesh = enemy.gameObject.GetComponent<NavMeshAgent>();
            navMesh.speed *= StatMod();
            agents.Add(navMesh);
            enemy._hpFill.color = Color.magenta;
        }

        StartCoroutine(ResetStats(agents));



    }







    public virtual float StatMod()
    {
        if (modifier == Modifier.Small)
        {
            return .8f;
        }
        else if (modifier == Modifier.Medium)
        {
            return .7f;
        }
        else
        {
            return .5f;
        }

    }


    public IEnumerator ResetStats(List<NavMeshAgent> agents)
    {
        yield return new WaitForSeconds(duration);
        foreach (NavMeshAgent agent in agents)
        {
            if(agent.gameObject.activeInHierarchy)
            {
                agent.speed /= StatMod();

                if(agent.speed == agent.gameObject.GetComponent<EnemyScript>().initialSpeed)
                {
                    agent.gameObject.GetComponent<EnemyScript>()._hpFill.color = Color.green;
                }
            }

        }

    }



}
