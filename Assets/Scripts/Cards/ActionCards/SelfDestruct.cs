﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SelfDestruct : DamageRandomEnemies
{


    [SerializeField]
    private int towersToDestroy;

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            UseAction();
        }

    }


    public override void UseAction()
    {
        TowerBase[] towers = FindObjectsOfType<TowerBase>();
        List<TowerBase> towerList = towers.ToList();
        if (towers.Length < towersToDestroy)
        {
            Debug.Log("Not enough towers to destroy");
            return;
        }
        else
        {
            Debug.Log("Ruba  dub");
        }

        EnemyScript[] enemies = FindObjectsOfType<EnemyScript>();

        Debug.Log("Enemies found is " + enemies.Length.ToString());

        if (enemies.Length == 0)
        {
            Debug.Log("No current enemies");
            return;
        }

        List<EnemyScript> enemyList = new List<EnemyScript>();
        List<EnemyScript> enemyStartingList = new List<EnemyScript>();

        enemyStartingList = enemies.ToList();

        int range;

        if (enemies.Length < enemyTargets)
        {
            range = enemies.Length;
        }
        else
        {
            range = enemyTargets;
        }

        for (int i = 0; i < range; i++)
        {
            EnemyScript entry = enemyStartingList[Random.Range(0, enemyStartingList.Count)];
            enemyList.Add(entry);
            enemyStartingList.Remove(entry);
        }

        StartCoroutine(CreateExplosions(enemyList));

        for (int i = 0; i < towersToDestroy; i++)
        {
            int rand = Random.Range(0, towerList.Count);
            towerList[rand].TakeDamage(1000);
            towerList.Remove(towerList[rand]);

        }
        


    }


    public override float DamageModifier()
    {
        return 1;


    }
}
