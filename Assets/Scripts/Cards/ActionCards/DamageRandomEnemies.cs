﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(AudioSource))]
public class DamageRandomEnemies : CardAbility {

    [SerializeField]
    protected int enemyTargets;

    [SerializeField]
    protected GameObject explosion;

    [SerializeField]
    protected AudioClip clip;

    [SerializeField]
    protected AudioSource audioPlayer;



    protected enum Damage
    {
        Small,
        Medium,
        Large

    }

    [SerializeField]
    protected Damage damage;


	// Use this for initialization
	void Start () {
        audioPlayer = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		

        if(Input.GetKeyDown(KeyCode.Q))
        {
            UseAction();
        }
	}


    public override void UseAction()
    {

        EnemyScript[] enemies = FindObjectsOfType<EnemyScript>();

        Debug.Log("Enemies found is " + enemies.Length.ToString());

        if(enemies.Length == 0)
        {
            Debug.Log("No current enemies");
            return;
        }

        List<EnemyScript> enemyList = new List<EnemyScript>();
        List<EnemyScript> enemyStartingList = new List<EnemyScript>();

        enemyStartingList = enemies.ToList();

        int range;

        if(enemies.Length < enemyTargets)
        {
            range = enemies.Length;
        }
        else
        {
            range = enemyTargets;
        }

        for (int i = 0; i < range; i++)
        {
            EnemyScript entry = enemyStartingList[Random.Range(0, enemyStartingList.Count)];
            enemyList.Add(entry);
            enemyStartingList.Remove(entry);
        }

        StartCoroutine(CreateExplosions(enemyList));
    }


    public IEnumerator CreateExplosions(List<EnemyScript> enemyList)
    {
        for (int i = 0; i < enemyList.Count; i++)
        {
            Debug.Log("Max hp is " + enemyList[i].GetMaxHP().ToString());

            float damage = (enemyList[i].GetMaxHP() * DamageModifier());

            GameObject obj = Instantiate(explosion, enemyList[i].transform.position, Quaternion.identity);
            audioPlayer.PlayOneShot(clip);



            enemyList[i].TakeDamage(damage);

            Debug.Log("Enemy has been dealt " + damage.ToString() + "damage");
            Destroy(obj, 1.5f);

            yield return new WaitForSeconds(.4f);
        }


    }




    public virtual float DamageModifier()
    {
        if(damage == Damage.Small)
        {
            return .3f;
        }
        else if(damage == Damage.Medium)
        {
            return .5f;
        }
        else
        {
            return .8f;
        }

    }



}
