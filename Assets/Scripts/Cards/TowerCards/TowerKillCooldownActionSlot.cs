﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerKillCooldownActionSlot : CardAbility {

    //Reduce the cooldown card in slot X by X seconds when enemy is killed


    [Tooltip ("This variable reduces the timer of the card's action cooldown in seconds")]
    [SerializeField]
    int seconds;
    [SerializeField]
    int actionCardSlot;

    public override void OnTowerKill()
    {
        //This card will use the int coolDownMod to reduce the Action card's cooldown
        Debug.Log("I called the on towerkill");

        ActionSlot actionSlot = gameObject.GetComponent<Card>().actionPalette.actionSlots[actionCardSlot];

        if(actionSlot.actionCard != null)
        {
            actionSlot.actionCard.GetComponent<ActionCard>().ReduceCooldown(seconds);
        }
        

    }

    public void Start()
    {
        actionCardSlot -= 1;

    }

    public void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            OnTowerKill();
        }
    }




}
