﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionCard : Card {

    //Card becomes active once cooldown has been finished, active at start of the game
    [SerializeField]
    private bool active = true;

    [SerializeField]
    private float timer = 0;

    public override void Awake()
    {
        coolDownSpeed = cardAsset.coolDownSpeed;
        coolDownMod = cardAsset.coolDownMod;
        coolDownSpeed *= coolDownMod;
        base.Awake();
    }




    public override void UseAbility()
    {
        if(deck.hand.handZoom.enlarged == true || deck.towerMenu.gameObject.activeInHierarchy == true)
        {
            return;
        }
        Debug.Log("I would use my ability");
        
        if(active == true)
        {
            active = false;
            StartCoroutine(CoolDown());
            Debug.Log("ability is used, now cooling down");
            this.gameObject.GetComponent<Button>().interactable = false;


            if(cardAbility != null)
            {
                cardAbility.UseAction();
            }
            
        }
        else
        {
            Debug.Log("Ability can't be used yet");
            Debug.Log("timer is " + (coolDownSpeed - timer).ToString());
        }



    }


    public IEnumerator CoolDown()
    {
        charges--;
        timer = 0;
        this.gameObject.GetComponent<Image>().color = Color.black;
        while (timer < coolDownSpeed && active == false)
        {
            timer += Time.deltaTime;
            //Debug.Log("timer is " + (coolDownSpeed - timer).ToString());
            this.gameObject.GetComponent<Image>().fillAmount = (1 - (timer / coolDownSpeed));
            yield return null;
        }
        timer = 0;
        active = true;
        Debug.Log("Ability can be used again");
        this.gameObject.GetComponent<Button>().interactable = true;
        this.gameObject.GetComponent<Image>().color = Color.white;
        this.gameObject.GetComponent<Image>().fillAmount = 1;
        if(charges == 0)
        {
            Debug.Log("card ran out of charges");
            this.gameObject.transform.parent.gameObject.GetComponent<ActionSlot>().RevertImage();
            this.gameObject.transform.parent.gameObject.GetComponent<ActionSlot>().actionCard = null;
            DiscardCard();
        }
        

    }

    public void ReduceCooldown(float seconds)
    {
        timer += seconds;
    }


}
