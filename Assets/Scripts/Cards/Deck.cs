﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;


public class Deck : MonoBehaviour {


    [SerializeField]
    private List<GameObject> cardDeck = new List<GameObject>();

    [SerializeField]
    private ActionPalette actionPalette;

    [SerializeField]
    private Graveyard graveyard;

    [SerializeField]
    private FieldSlot fieldSlot;

    [SerializeField]
    public TowerMenu towerMenu;

    [SerializeField]
    private AudioSource cardDeal;
   



    [SerializeField]
    private GameObject cardHand;


    [SerializeField]
    public Hand hand;

    [SerializeField]
    public float coolDown;

    [SerializeField]
    private float timer = 0;

    [SerializeField]
    private int cardDrawMax;

    [SerializeField]
    private int drawCount;

    [SerializeField]
    private TextMeshProUGUI availableCards;

    [SerializeField]
    private Image radialCooldown;

    [SerializeField]
    private Button drawCardButton;

    [SerializeField]
    private bool isRunningCooldown;


    public void Awake()
    {


    }

    // Use this for initialization
    void Start () {


        availableCards.text = drawCount.ToString();
        drawCardButton.interactable = true;
        isRunningCooldown = false;
        cardDeal = GetComponent<AudioSource>();

        if (!File.Exists(Application.persistentDataPath + "/" + "GameDeck" + ".xml"))
        {
            PersistentCardDatabase db = FindObjectOfType<PersistentCardDatabase>();
            List<GameObject> defaultDeck = new List<GameObject>();
            defaultDeck = db.defaultDeck.cardList;
            Debug.Log("Using default deck, writing default deck to GameDeck.xml");
            DeckUtils.WriteDeck(defaultDeck, "GameDeck");
            cardDeck = defaultDeck;
        }
        else
        {
            DeckUtils.ReadDeck(cardDeck, "GameDeck");
            Debug.Log("Player's deck was found, using player's custom deck for this game");
        }


        foreach (GameObject card in cardDeck)
        {
            card.GetComponent<Card>().actionPalette = actionPalette;
            card.GetComponent<Card>().hand = hand;
            card.GetComponent<Card>().graveyard = graveyard;
            card.GetComponent<Card>().fieldSlot = fieldSlot;
            card.GetComponent<Card>().cardLocation = CardLocation.Deck;
            card.GetComponent<Card>().deck = this;
        }
        DeckUtils.ShuffleDeck(cardDeck);
        
	}
	
	// Update is called once per frame
	void Update () {

        DrawCard();

	}


    /// <summary>
    /// This method will draw a card if the number of cards available to draw is zero,
    /// it will start a counter and increase the number of available cards to draw each cycle
    /// </summary>
    public void DrawCard()
    {
        if(hand.isSelectingActionCard == true || hand.isSelectingFieldCard == true || hand.isSelectingTowerCard == true)
        {
            return;
        }
        if(cardDeck.Count < 1)
        {
            DeckUtils.ShuffleDeck(graveyard.graveyard); 
            foreach(GameObject card in graveyard.graveyard)
            {
                card.GetComponent<Card>().cardLocation = CardLocation.Deck;
                cardDeck.Add(card);
                Debug.Log("Graveyard card added to deck");
            }
            graveyard.graveyard.Clear();
        }

       if(drawCount > 0 && cardDeck.Count > 0)
        {
            if(hand.cardHand.Count >= 5)
            {

                return;
            }
            drawCount--;
            availableCards.text = drawCount.ToString();
            if(drawCount == 0)
            {
                drawCardButton.interactable = false;
            }
            if(isRunningCooldown == false)
            {
                StartCoroutine(drawCoolDown());
            }
            cardDeal.Play();

            GameObject newCard = Instantiate(cardDeck[0], cardHand.transform);
            cardDeck.RemoveAt(0);
            newCard.GetComponent<Card>().cardLocation = CardLocation.Hand;
            hand.cardHand.Add(newCard);
            
        }
        
        
    }



    public IEnumerator drawCoolDown()
    {
        isRunningCooldown = true;
        timer = 0;
        while (timer < coolDown)
        {
            timer += Time.deltaTime;

            radialCooldown.fillAmount = (1 - (timer / coolDown));
            yield return null;
        }
        timer = 0;
        radialCooldown.fillAmount = 0;
        drawCount = ((drawCount + 1) < cardDrawMax) ? (drawCount + 1) : cardDrawMax;
        drawCardButton.interactable = true;
        availableCards.text = drawCount.ToString();
        Debug.Log("drawcount is " + drawCount.ToString());
        isRunningCooldown = false;
        if(drawCount < cardDrawMax)
        {
            StartCoroutine(drawCoolDown());
        }
    }

}
