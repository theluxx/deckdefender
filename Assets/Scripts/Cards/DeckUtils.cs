﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Linq;

static class DeckUtils {




    public static void ShuffleDeck(List<GameObject> deck)
    {
        // Loops through array
        for (int i = deck.Count - 1; i > 0; i--)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = Random.Range(0, i);

            // Save the value of the current i, otherwise it'll overright when we swap the values
            GameObject temp = deck[i];

            // Swap the new and old values
            deck[i] = deck[rnd];
            deck[rnd] = temp;
        }
    }


    public static void WriteDeck(List<GameObject> cardList)
    {
        // The quickest way to write out a value is to player refs, this is not the most correct way however.
        //PlayerPrefs.SetInt ("HighScore", myScore);

        List<string> cardID = new List<string>();

        foreach(GameObject card in cardList)
        {
            cardID.Add(card.GetComponent<Card>().cardAsset.GetCardID());
        }

        // We create an xml document which we will begin populating.
        XDocument document = new XDocument();
        // Create a new element called HighScore which we will store in our file.

        


        document.Add(new XElement("PlayerDeck"));
        foreach (string card in cardID)
        {

            // Add our current score to the element.
            document.Element("PlayerDeck").Add(new XElement("Card", card));
            // Append our element to the document we have created.

        }
        // We save out our document, if we already have a file at this path it will overwrite it.
        document.Save(Application.persistentDataPath + "/PlayerDeck.xml");

        Debug.Log("File was saved");
    }

    public static void WriteDeck(List<GameObject> cardList, string location)
    {
        // The quickest way to write out a value is to player refs, this is not the most correct way however.
        //PlayerPrefs.SetInt ("HighScore", myScore);

        List<string> cardID = new List<string>();

        foreach (GameObject card in cardList)
        {
            cardID.Add(card.GetComponent<Card>().cardAsset.GetCardID());
        }

        // We create an xml document which we will begin populating.
        XDocument document = new XDocument();
        // Create a new element called HighScore which we will store in our file.




        document.Add(new XElement("PlayerDeck"));
        foreach (string card in cardID)
        {

            // Add our current score to the element.
            document.Element("PlayerDeck").Add(new XElement("Card", card));
            // Append our element to the document we have created.

        }
        // We save out our document, if we already have a file at this path it will overwrite it.
        document.Save(Application.persistentDataPath + "/" + location + ".xml");

        Debug.Log("File was saved to " + location);
    }


    //This requires the gameobject PersistantCardDatabase to exist in the scene
    public static void ReadDeck(List<GameObject> myList)
    {
        
        //TextAsset PlayerDeck = File.Open(Application.persistentDataPath + "/PlayerDeck.xml", FileMode.Open) as TextAsset;
        //XDocument myData = XDocument.Parse(PlayerDeck.text);

        XDocument myData = new XDocument(XDocument.Load(Application.persistentDataPath + "/PlayerDeck.xml"));

        PersistentCardDatabase data = GameObject.FindObjectOfType<PersistentCardDatabase>().GetComponent<PersistentCardDatabase>();

        List<string> stringList = new List<string>();
        List<string> cardID = new List<string>();
        List<GameObject> cardObject = new List<GameObject>();



        var newList = myData.Element("PlayerDeck").Elements()
                           .Select(element => element.Value)
                           .ToList();

        foreach(var item in newList)
        {

            stringList.Add(item.ToString());
        }

        int index = 0;

        myList.Clear();
        foreach(string identifier in stringList)
        {
            index = data.cardIDs.FindIndex(a => a.Contains(identifier));
            myList.Add(data.cardObjects[index]);
        }

        

    }

    //This requires the gameobject PersistantCardDatabase to exist in the scene
    public static void ReadDeck(List<GameObject> myList, string location)
    {

        //TextAsset PlayerDeck = File.Open(Application.persistentDataPath + "/PlayerDeck.xml", FileMode.Open) as TextAsset;
        //XDocument myData = XDocument.Parse(PlayerDeck.text);

        XDocument myData = new XDocument(XDocument.Load(Application.persistentDataPath + "/" + location + ".xml"));

        PersistentCardDatabase data = GameObject.FindObjectOfType<PersistentCardDatabase>().GetComponent<PersistentCardDatabase>();

        List<string> stringList = new List<string>();
        List<string> cardID = new List<string>();
        List<GameObject> cardObject = new List<GameObject>();



        var newList = myData.Element("PlayerDeck").Elements()
                           .Select(element => element.Value)
                           .ToList();

        foreach (var item in newList)
        {

            stringList.Add(item.ToString());
        }

        int index = 0;

        myList.Clear();
        foreach (string identifier in stringList)
        {
            index = data.cardIDs.FindIndex(a => a.Contains(identifier));
            myList.Add(data.cardObjects[index]);
        }

        Debug.Log("File was read from " + location);

    }


}
