﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum CardLocation
{
        Deck = 0,
        Hand = 1,
        ActionPalette = 2,
        FieldSlot = 3,
        TowerSlot = 4,
        Graveyard = 5

}




public abstract class Card : MonoBehaviour {



    //Tower this card is equipped to
    public TowerBase tower;

    //Card asset that it derives it's values from
    [SerializeField]
    public CardAsset cardAsset;

    [SerializeField]
    public CardAbility cardAbility;

    [SerializeField]
    public ActionPalette actionPalette;
    [SerializeField]
    public FieldSlot fieldSlot;
    [SerializeField]
    public Hand hand;
    [SerializeField]
    public Graveyard graveyard;

    [SerializeField]
    public Deck deck;


    //Card's name
    [SerializeField]
    private string cardName;

    [SerializeField]
    private Image cardImage;

    //Card's description
    [SerializeField]
    [TextArea(2, 3)]
    private string cardDescription;

    [SerializeField]
    public TextMeshProUGUI cardNameText;

    [SerializeField]
    public TextMeshProUGUI cardDescriptText;

    [SerializeField]
    public Button thisButton;

    [SerializeField]
    public CardLocation cardLocation;


    



    //Modifier stats
    public float cardHp = 1;
    public float cardDmg = 1;
    public float cardSpd = 1;
    public float scrapGainMod = 1;
    public float upgradeCostMod = 1;
    public float scoreMod = 1;
    public float attackRangeMod = 1;
    public float coolDownMod = 1;
    public int charges = 1;

    //Card cooldown speed pertains to action cards
    public float coolDownSpeed = 10;


    public virtual void Awake()
    {

        thisButton = this.gameObject.GetComponent<Button>();
        cardName = cardAsset.cardName;
        cardDescription = cardAsset.cardDescription;

        if (cardNameText != null)
        {
            cardNameText.SetText(cardName);
            cardDescriptText.SetText(cardDescription);
        }

        if(this.gameObject.GetComponent<CardAbility>() != null)
        {
            cardAbility = this.gameObject.GetComponent<CardAbility>();
            cardAbility.card = this;
        }






        cardHp = cardAsset.cardHp;
        cardDmg = cardAsset.cardDmg;
        cardSpd = cardAsset.cardSpd;
        scrapGainMod = cardAsset.scrapCostMod;
        upgradeCostMod = cardAsset.upgradeCostMod;
        scoreMod = cardAsset.scoreMod;
        attackRangeMod = cardAsset.attackRangeMod;
        charges = cardAsset.charges;
        if(cardAsset.cardSprite != null)
        {
            cardImage.sprite = cardAsset.cardSprite;
        }
        


    }

    // Use this for initialization
    public virtual void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void UseAbility()
    {

    }

    public virtual void DiscardCard()
    {
        cardLocation = CardLocation.Graveyard;
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        gameObject.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        graveyard.graveyard.Add(Instantiate(this.gameObject));
        StartCoroutine(MoveToGraveyard());
    }



    /// <summary>
    /// This method is called when the player clicks on the card that is in the hand
    /// </summary>
    public void OnHandClick()
    {
        if(this.gameObject.transform.parent.gameObject.GetComponent<Hand>())
        {

            if (hand.isSelectingActionCard)
            {
                hand.EquipFromHandToActionSlot(this.transform.GetSiblingIndex(), hand.slotToEquip);
            }
            if(hand.isSelectingFieldCard)
            {
                hand.EquipFromHandToFieldSlot(this.transform.GetSiblingIndex(), hand.fieldSlot);
            }
            if(hand.isSelectingTowerCard)
            {
                hand.EquipFromHandToTowerSlot(this.transform.GetSiblingIndex(), FindObjectOfType<TowerMenu>().tower);
            }



        }
        else
        {
            if (cardLocation == CardLocation.ActionPalette)
            {
                this.gameObject.GetComponent<ActionCard>().UseAbility();
            }
        }

        
    }

    public IEnumerator MoveToGraveyard()
    {
        Debug.Log("Moving to graveyard");
        float percent = 0;
        while(percent < 1)
        {
            percent += (Time.deltaTime * .3f);
            this.gameObject.transform.position = Vector3.Lerp(this.gameObject.transform.position, graveyard.transform.position, percent);
            yield return null;
        }
        Destroy(this.gameObject, Time.deltaTime);
    }

}
