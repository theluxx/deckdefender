﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardAsset : ScriptableObject {


    public CardAbilityAsset onEquipAbility;

    protected enum CardCategory
    {
        TWR,
        FLD,
        ACT
    };

    public enum Rarity
    {
        Normal,
        Rare,
        Epic
    }

    [SerializeField]
    protected CardCategory _cat;
    [SerializeField]
    protected int _idNum;

    public string GetCardID()
    {
        string cardID = _cat.ToString() + _idNum.ToString();
        return cardID;
    }




    //Card's name
    [SerializeField]
    public string cardName;

    [SerializeField]
    public Sprite cardSprite;

    public Rarity rarity;

    //Card's description
    [TextArea(2, 3)]
    public string cardDescription;

    //Modifier stats
    public float cardHp = 1;
    public float cardDmg = 1;
    public float cardSpd = 1;
    public float scrapCostMod = 1;
    public float upgradeCostMod = 1;
    public float scoreMod = 1;
    public float attackRangeMod = 1;
    public float coolDownMod = 1;
    public int charges = 1;



    //Action Card
    public float coolDownSpeed;

    public void Awake()
    {
   
        

    }


    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

}
