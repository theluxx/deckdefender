﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeeklyDistributor : MonoBehaviour {

    [SerializeField]
    private List<CardList> weeklyList;

    [SerializeField]
    private CardList epicList;

    [SerializeField]
    private CardList rareList;

    [SerializeField]
    private CardList commonList;

    [SerializeField]
    private CardList persistentList;

    [SerializeField]
    private List<GameObject> totalList = new List<GameObject>();

    // Use this for initialization
    void Start () {

        //totalList = persistentList.cardList;

        //foreach(GameObject obj in epicList.cardList)
        //{
        //    totalList.Remove(obj);
        //}
        //foreach (GameObject obj in rareList.cardList)
        //{
        //    totalList.Remove(obj);
        //}

        //commonList.cardList = totalList;

        Debug.Log("Total list culled to normal cards");

        epicList.cardList.Clear();
        rareList.cardList.Clear();
        commonList.cardList.Clear();

        foreach(GameObject obj in persistentList.cardList)
        {
            if(obj.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Epic)
            {
                epicList.cardList.Add(obj);
            }
            if (obj.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
            {
                rareList.cardList.Add(obj);
            }
            if (obj.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
            {
                commonList.cardList.Add(obj);
            }

        }


        List<GameObject> _epic = new List<GameObject>();
        List<GameObject> _rare = new List<GameObject>();
        List<GameObject> _common = new List<GameObject>();

        foreach(GameObject obj in epicList.cardList)
        {
            _epic.Add(obj);
        }
        foreach (GameObject obj in rareList.cardList)
        {
            _rare.Add(obj);
        }
        foreach (GameObject obj in commonList.cardList)
        {
            _common.Add(obj);
        }



        foreach (CardList cList in weeklyList)
        {
            cList.cardList.Clear();
            int randEpic = Random.Range(0, _epic.Count);
            cList.cardList.Add(_epic[randEpic]);
            _epic.Remove(_epic[randEpic]);

            for (int i = 0; i < 2; i++)
            {
                int randRare = Random.Range(0, _rare.Count);
                cList.cardList.Add(_rare[randRare]);
                _rare.Remove(_rare[randRare]);

            }
            for (int i = 0; i < 7; i++)
            {
                int randCommon = Random.Range(0, _common.Count);
                cList.cardList.Add(_common[randCommon]);
                _common.Remove(_common[randCommon]);

            }
        }

        Debug.Log("All weekly lists created");




    }
	

}
