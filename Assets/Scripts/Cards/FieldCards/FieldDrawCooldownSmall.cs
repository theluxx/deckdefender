﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldDrawCooldownSmall : CardAbility {

    public float drawTimeReduction;

    public Deck deck;

    public override void Start()
    {
        drawTimeReduction = 2;
    }


    public override void OnFieldEquip()
    {
        Debug.Log("I lowered the cool down");
        deck = FindObjectOfType<Deck>();
        deck.coolDown -= drawTimeReduction;

        card.UseAbility();
    }


    public override void OnFieldDiscard()
    {
        deck.coolDown += drawTimeReduction;
        card.DiscardCard();
    }


}
