﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseFieldStatsTemp : IncreaseStatTemp
{




    // Use this for initialization
    public override void Start()
    {
        if (FindObjectOfType<Hand>() != null)
        {
            towerMenu = FindObjectOfType<Hand>().towerMenu;
        }
        mod = StatMod();
        
    }




    public override void OnFieldEquip()
    {
        Debug.Log("This is being called");

        TowerBase[] towers = FindObjectsOfType<TowerBase>();
        List<TowerBase> towerList = new List<TowerBase>();

        if (towers.Length == 0)
        {
            return;
        }


        foreach (TowerBase tower in towers)
        {
            towerList.Add(tower);
            if (stat == Stat.Attack)
            {
                tower.ActionCardModAtk(mod, true);
            }
            if (stat == Stat.Range)
            {
                tower.ActionCardModRange(mod, true);
            }
            if (stat == Stat.Speed)
            {
                tower.ActionCardModSpeed(mod, true);
            }



            if (towerMenu.gameObject.activeInHierarchy)
            {
                if(towerMenu.tower != null)
                {
                    towerMenu.SetCurrentTowerTexts();
                }
                
            }


            StartCoroutine(ResetStats(towerList));

        }

        card.UseAbility();



    }







    public override float StatMod()
    {
        if (modifier == Modifier.Small)
        {
            return 1.05f;
        }
        else if (modifier == Modifier.Medium)
        {
            return 1.1f;
        }
        else
        {
            return 1.15f;
        }

    }





}
