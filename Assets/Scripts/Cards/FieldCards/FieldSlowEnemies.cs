﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class FieldSlowEnemies : SlowEnemies {




    public override void OnFieldEquip()
    {
        EnemyScript[] enemies = FindObjectsOfType<EnemyScript>();
        List<NavMeshAgent> agents = new List<NavMeshAgent>();
        if (enemies.Length == 0)
        {
            return;
        }


        foreach (EnemyScript enemy in enemies)
        {
            NavMeshAgent navMesh = enemy.gameObject.GetComponent<NavMeshAgent>();
            navMesh.speed *= StatMod();
            agents.Add(navMesh);
        }

        StartCoroutine(ResetStats(agents));
    }

    public override float StatMod()
    {
        if (modifier == Modifier.Small)
        {
            return .9f;
        }
        else if (modifier == Modifier.Medium)
        {
            return .8f;
        }
        else
        {
            return .7f;
        }

    }

}
