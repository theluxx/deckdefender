﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSlotsCooldownSmall : CardAbility {

    public int reduceCoolDownGlobal;
    public List<ActionCard> activeCards = new List<ActionCard>();


    // Use this for initialization
    void Start()
    {
        reduceCoolDownGlobal = 5;
    }

    public override void OnFieldEquip()
    {
        Debug.Log("I used the cooldown thing");
        ActionSlot[] slots = FindObjectsOfType<ActionSlot>();

        foreach (ActionSlot slot in slots)
        {
            if (slot.actionCard != null)
            {
                activeCards.Add(slot.actionCard.GetComponent<ActionCard>());
                float coolDown = slot.actionCard.GetComponent<ActionCard>().coolDownSpeed;

                coolDown -= reduceCoolDownGlobal;

                if (coolDown <= 0)
                {
                    coolDown = 5;
                }

                slot.actionCard.GetComponent<ActionCard>().coolDownSpeed = coolDown;
            }
        }
        card.UseAbility();

    }

    public override void OnFieldDiscard()
    {
        Debug.Log("I changed back all the values");
        foreach (ActionCard card in activeCards)
        {

            card.coolDownSpeed += reduceCoolDownGlobal;
        }
        activeCards.Clear();
        card.DiscardCard();
    }
}
