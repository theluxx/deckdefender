﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCard : Card {

    public override void Awake()
    {

        base.Awake();
    }


	


    /// <summary>
    /// This method is called when a card is equipped to a tower, most usually augment's a towers stat modifiers
    /// </summary>
    public virtual void OnTowerEquip()
    {
        if(cardAbility != null)
        {
            cardAbility.OnTowerEquip();
        }
        
    }

    /// <summary>
    /// This method is called when the tower this card is equipped to is destroyed
    /// </summary>
    public virtual void OnTowerDestroy()
    {
        if (cardAbility != null)
        {
            cardAbility.OnTowerDestroy();
        }
        else
        {
            Debug.Log("I called else");
            DiscardCard();
        }
    }

    public virtual void OnTowerDiscard()
    {
        if (cardAsset.onEquipAbility != null)
        {
            cardAsset.onEquipAbility.OnTowerDiscard();
        }

    }

    /// <summary>
    /// This method is called when the tower this card is equipped to kills an enemy unit
    /// </summary>
    public virtual void OnTowerKill()
    {
        if (cardAbility != null)
        {
            Debug.Log(" something something something");
            cardAbility.OnTowerKill();
        }
    }

    /// <summary>
    /// This method is called when a card is equipped to a tower card slot which already has a card equipped in it
    /// </summary>
    public virtual void OnTowerTribute()
    {

        if (cardAsset.onEquipAbility != null)
        {
            cardAsset.onEquipAbility.OnTowerTribute();
        }
    }

    /// <summary>
    /// This method is called to change the stat modifiers on the tower this card is equipped to
    /// </summary>
    public void ModValues()
    {
        Debug.Log("I would have exited the state");
        //tower.ChangeModValues(this);
    }

    public override void DiscardCard()
    {
        cardLocation = CardLocation.Graveyard;
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        gameObject.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
        graveyard.graveyard.Add(Instantiate(this.gameObject));

    }

}
