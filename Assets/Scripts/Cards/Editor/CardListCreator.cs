﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CardListCreator : MonoBehaviour {

    [MenuItem("Assets/Create/CardList")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility.CreateAsset<CardList>();
    }
}
