﻿using UnityEngine;
using UnityEditor;

static class CardAssetCreator
{

    [MenuItem("Assets/Create/CardAsset")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility.CreateAsset<CardAsset>();
    }

}