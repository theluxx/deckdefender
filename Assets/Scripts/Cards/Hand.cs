﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Xml;
using System.Xml.Linq;

public class Hand : MonoBehaviour {

    [SerializeField]
    public List<GameObject> cardHand = new List<GameObject>();

    [SerializeField]
    public ActionPalette actionPalette;

    [SerializeField]
    public FieldSlot fieldSlot;

    [SerializeField]
    public HandZoom handZoom;

    [SerializeField]
    public Deck deck;

    [SerializeField]
    public bool isSelectingActionCard;

    [SerializeField]
    public bool isSelectingFieldCard;

    [SerializeField]
    public bool isSelectingTowerCard;

    [SerializeField]
    public int slotToEquip;

    [SerializeField]
    public Animator handAnim;

    [SerializeField]
    public CancelButton cancelButton;

    [SerializeField]
    public int towerSlotToEquip;

    [SerializeField]
    public TowerMenu towerMenu;

    [SerializeField]
    private GameObject cancelPanel;

    [SerializeField]
    private AudioSource cardPlace;


    public void Awake()
    {
        handAnim = this.gameObject.GetComponent<Animator>();
        cardPlace = this.gameObject.GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
		
	}
	




    public void EquipFromHandToActionSlot(int card, int slot)
    {

        cardHand[card].transform.position = actionPalette.actionSlots[slot].transform.position;
        cardHand[card].transform.SetParent(actionPalette.actionSlots[slot].gameObject.transform);
        cardHand[card].gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        actionPalette.actionSlots[slot].actionCard = cardHand[card];
        actionPalette.actionSlots[slot].gameObject.GetComponent<Image>().sprite = cardHand[card].GetComponent<Image>().sprite;
        cardHand[card].GetComponent<Card>().cardLocation = CardLocation.ActionPalette;
        cardHand.Remove(cardHand[card]);
        handZoom.enlarged = false;
        handAnim.SetTrigger("Shrink");
        isSelectingActionCard = false;
        isSelectingFieldCard = false;
        isSelectingTowerCard = false;

        cardPlace.Play();
        for(int i = 0; i < cardHand.Count; i++)
        {
            cardHand[i].GetComponent<Button>().interactable = true;
        }
        handZoom.gameObject.GetComponent<Button>().interactable = true;
    }

    //To complete when towers done
    public void EquipFromHandToTowerSlot(int card, TowerBase tower)
    {
        
        TowerCard[] cards = tower.GetEquippedCards();

        int cardsEquipped = 0;
        for (int i = 0; i < tower.GetLevel(); i++)
        {
            if (cards[i] != null)
            {
                cardsEquipped++;
            }
        }

        if(cardsEquipped == tower.GetLevel())
        {
            return;
        }

        for (int i = 0; i < cards.Length; i++)
        {
            if(cards[i] == null)
            {
                towerSlotToEquip = i;
                break;
            }
        }

        cardHand[card].transform.position = towerMenu.cardSlots[towerSlotToEquip].transform.position;
        cardHand[card].transform.SetParent(towerMenu.cardSlots[towerSlotToEquip].transform);
        cardHand[card].gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        tower.EquipCard(cardHand[card].GetComponent<TowerCard>(), towerSlotToEquip);
        cardHand[card].GetComponent<Card>().cardLocation = CardLocation.TowerSlot;
        cardHand[card].GetComponent<TowerCard>().OnTowerEquip();
        cardHand.Remove(cardHand[card]);

        towerMenu.SetCurrentTowerTexts();

        cardsEquipped = 0;
        for (int i = 0; i < tower.GetLevel(); i++)
        {
            if (cards[i] != null)
            {
                cardsEquipped++;
            }
        }

        cardPlace.Play();

        if (cardsEquipped == tower.GetLevel())
        {
            this.transform.SetParent(towerMenu.gameObject.transform.parent);
            handZoom.enlarged = false;
            handAnim.SetTrigger("Shrink");
            isSelectingFieldCard = false;
            isSelectingActionCard = false;
            isSelectingTowerCard = false;

            for (int i = 0; i < cardHand.Count; i++)
            {
                cardHand[i].GetComponent<Button>().interactable = true;
            }
            handZoom.gameObject.GetComponent<Button>().interactable = true;
            towerMenu.cardSlots[towerSlotToEquip].interactable = false;
            Debug.Log("card slots are full");
        }


    }

    public void EquipFromHandToFieldSlot(int card, FieldSlot slot)
    {

        cardHand[card].transform.position = slot.transform.position;
        cardHand[card].transform.SetParent(slot.transform);
        cardHand[card].gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        slot.fieldCard = cardHand[card];
        slot.gameObject.GetComponent<Image>().sprite = cardHand[card].GetComponent<Image>().sprite;
        cardHand[card].GetComponent<Card>().cardLocation = CardLocation.FieldSlot;
        cardHand[card].GetComponent<FieldCard>().OnFieldEquip();
        cardHand.Remove(cardHand[card]);
        handZoom.enlarged = false;
        handAnim.SetTrigger("Shrink");
        isSelectingFieldCard = false;
        isSelectingActionCard = false;
        isSelectingTowerCard = false;

        cardPlace.Play();
        for (int i = 0; i < cardHand.Count; i++)
        {
            cardHand[i].GetComponent<Button>().interactable = true;
        }
        handZoom.gameObject.GetComponent<Button>().interactable = true;
    }

    public void OnActionClick(GameObject actionCard, Hand hand, ActionSlot actionSlot)
    {
        if (actionCard == null && handZoom.enlarged == false)
        {
            cancelPanel.SetActive(true);
            handAnim.SetTrigger("Enlarge");
            handZoom.enlarged = true;
            hand.isSelectingActionCard = true;
            hand.slotToEquip = actionSlot.transform.GetSiblingIndex();
            handZoom.gameObject.GetComponent<Button>().interactable = false;
            foreach (GameObject obj in hand.cardHand)
            {
                if (obj.GetComponent<ActionCard>())
                {
                    obj.GetComponent<Button>().interactable = true;
                }
                else
                {
                    obj.GetComponent<Button>().interactable = false;
                }
            }
        }
    }



    /// <summary>
    /// This overloaded method might be obsolete now
    /// </summary>
    /// <param name="towerCard"></param>
    /// <param name="cardNum"></param>
    /// <param name="equipSlot"></param>
    public void OnTowerClick(TowerCard[] towerCard, int cardNum, int equipSlot)
    {
        if (towerCard[cardNum] == null && handZoom.enlarged == false)
        {
            handAnim.SetTrigger("Enlarge");
            handZoom.enlarged = true;
            isSelectingTowerCard = true;
            towerSlotToEquip = equipSlot;
           // hand.slotToEquip = actionSlot.transform.GetSiblingIndex();
            handZoom.gameObject.GetComponent<Button>().interactable = false;
            foreach (GameObject obj in cardHand)
            {
                if (obj.GetComponent<TowerCard>())
                {
                    obj.GetComponent<Button>().interactable = true;
                }
                else
                {
                    obj.GetComponent<Button>().interactable = false;
                }
            }
            
        }
    }


    //Currently used by tower menu
    public void OnTowerClick()
    {
        if (handZoom.enlarged == false)
        {
            handAnim.SetTrigger("Enlarge");
            handZoom.enlarged = true;
            isSelectingTowerCard = true;
            handZoom.gameObject.GetComponent<Button>().interactable = false;
            foreach (GameObject obj in cardHand)
            {
                if (obj.GetComponent<TowerCard>())
                {
                    obj.GetComponent<Button>().interactable = true;
                }
                else
                {
                    obj.GetComponent<Button>().interactable = false;
                }
            }

        }
    }



    public void OnFieldClick(GameObject fieldCard, Hand hand, FieldSlot fieldSlot)
    {
        if (fieldCard == null && handZoom.enlarged == false)
        {
            cancelPanel.SetActive(true);
            handAnim.SetTrigger("Enlarge");
            handZoom.enlarged = true;
            hand.isSelectingFieldCard = true;
            handZoom.gameObject.GetComponent<Button>().interactable = false;
            foreach (GameObject obj in hand.cardHand)
            {
                if (obj.GetComponent<FieldCard>())
                {
                    obj.GetComponent<Button>().interactable = true;
                }
                else
                {
                    obj.GetComponent<Button>().interactable = false;
                }
            }
        }
    }


}
