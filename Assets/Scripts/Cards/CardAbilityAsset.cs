﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardAbilityAsset : ScriptableObject
{

    public string name;
    public string description;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// This method is called when a card is equipped to a tower, most usually augment's a towers stat modifiers
    /// </summary>
    public virtual void OnTowerEquip()
    {

    }

    /// <summary>
    /// This method is called when the tower this card is equipped to is destroyed
    /// </summary>
    public virtual void OnTowerDestroy()
    {
        
    }

    /// <summary>
    /// This method is called when the tower this card is equipped to kills an enemy unit
    /// </summary>
    public virtual void OnTowerKill()
    {

    }

    /// <summary>
    /// This method is called when a card is equipped to a tower card slot which already has a card equipped in it
    /// </summary>
    public virtual void OnTowerTribute()
    {


    }

    public virtual void OnTowerDiscard()
    {


    }

    public virtual void OnFieldEquip()
    {
       
    }

    public virtual void OnFieldDiscard()
    {

    }

}
