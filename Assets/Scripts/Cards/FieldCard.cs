﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldCard : Card {




    public virtual void OnFieldEquip()
    {
        if(cardAbility != null)
        {
            cardAbility.OnFieldEquip();
        }
        else
        {
            Debug.LogError("No card ability component attached to this card gameobject");
        }
        
        UseAbility();
    }

    public virtual void OnFieldDiscard()
    {
        if (cardAbility != null)
        {
            cardAbility.OnFieldDiscard();
        }
        DiscardCard();
    }

    //Card becomes active once cooldown has been finished, active at start of the game
    [SerializeField]
    private bool active = true;

    [SerializeField]
    private float timer = 0;

    public override void Awake()
    {
        coolDownSpeed = cardAsset.coolDownSpeed;
        coolDownMod = cardAsset.coolDownMod;
        coolDownSpeed *= coolDownMod;
        base.Awake();
    }




    public override void UseAbility()
    {
        Debug.Log("I would use my ability");

        if (active == true)
        {
            active = false;
            StartCoroutine(CoolDown());
            Debug.Log("ability is used, now cooling down");
            this.gameObject.GetComponent<Button>().interactable = false;

        }
        else
        {
            Debug.Log("Ability can't be used yet");
            Debug.Log("timer is " + (coolDownSpeed - timer).ToString());
        }
    }


    public IEnumerator CoolDown()
    {
        charges--;
        timer = 0;
        this.gameObject.GetComponent<Image>().color = Color.black;
        while (timer < coolDownSpeed && active == false)
        {
            timer += Time.deltaTime;
            //Debug.Log("timer is " + (coolDownSpeed - timer).ToString());
            this.gameObject.GetComponent<Image>().fillAmount = (1 - (timer / coolDownSpeed));
            yield return null;
        }
        timer = 0;
        active = true;
        Debug.Log("Ability can be used again");
        this.gameObject.GetComponent<Button>().interactable = true;
        this.gameObject.GetComponent<Image>().color = Color.white;
        this.gameObject.GetComponent<Image>().fillAmount = 1;
        if (charges == 0)
        {
            Debug.Log("card ran out of charges");
            this.gameObject.transform.parent.gameObject.GetComponent<FieldSlot>().RevertImage();
            this.gameObject.transform.parent.gameObject.GetComponent<FieldSlot>().fieldCard = null;
            OnFieldDiscard();
        }


    }

    public void ReduceCooldown(float seconds)
    {
        timer += seconds;
    }



}
