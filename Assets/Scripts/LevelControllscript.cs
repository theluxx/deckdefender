﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelControllscript : MonoBehaviour {

	public static LevelControllscript instance = null;
	GameObject levelSign;
	int sceneIndex,levelPassed;

	// Use this for initialization
	void Start ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);


		levelSign = GameObject.Find ("LevelNumber");

		sceneIndex = SceneManager.GetActiveScene ().buildIndex;
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void youWin()
	{
		if(sceneIndex == 3)
			Invoke ("loadMainMenu", 1f);
		else {
			if (levelPassed < sceneIndex)
				PlayerPrefs.SetInt ("LevelPassed", sceneIndex);
			Invoke ("loadNExtLevel", 1f);
		}
	}

	public void youLose()
	{
		Invoke ("loadMainMenu", 1f);

	}

	public void loadNextLevel()
	{
		SceneManager.LoadScene (sceneIndex + 1);
    }

    public void Restart()
    {
        SceneManager.LoadScene(sceneIndex);

    }

    void loadMainMenu()
	{
		SceneManager.LoadScene (1);
	}

    public void CloseGame()
    {
        Application.Quit();
    }

    
}
