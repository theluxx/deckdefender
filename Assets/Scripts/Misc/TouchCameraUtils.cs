﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCameraUtils : MonoBehaviour {


    [SerializeField]
    private float keyFloat;

    [SerializeField]
    private float cameraMax;

    [SerializeField]
    private float cameraMin;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Camera camera;

    [SerializeField]
    private float perspectiveMin;

    [SerializeField]
    private float perspectiveMax;


    [SerializeField]
    private float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
    [SerializeField]
    private float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.


    [SerializeField]
    private float xMin;

    [SerializeField]
    private float xMax;

    [SerializeField]
    private float yMin;

    [SerializeField]
    private float yMax;

    [SerializeField]
    private float zMin;

    [SerializeField]
    private float zMax;


    // Use this for initialization
    void Start () {

#if UNITY_EDITOR
        Debug.Log("This was unity editor");
#endif

        camera = Camera.main;

        if(perspectiveMin == null || perspectiveMin == 0)
        {
            Debug.Log("Perspective min set to 20 as no value was entered in inspector");
            perspectiveMin = 20;
        }


        if(perspectiveMax == null || perspectiveMax == 0)
        {
            Debug.Log("Perspective max set to 60 as no value was entered in inspector");
            perspectiveMax = 60;
        }

        perspectiveMin = Mathf.Clamp(perspectiveMin, .1f, 179.9f);
        perspectiveMax = Mathf.Clamp(perspectiveMax, .1f, 179.9f);

        if (perspectiveMin > perspectiveMax || perspectiveMin == perspectiveMax)
        {
            Debug.LogError("Perspective min and max defaulted to 20 and 60 due to invalid entry numbers");
            perspectiveMin = 20;
            perspectiveMax = 60;
        }


    }
	
	// Update is called once per frame
	void Update () {


        KeyMovement();

#if UNITY_ANDROID
        TouchMovement();
#endif

        float x = this.transform.position.x;
        float y = this.transform.position.y;
        float z = this.transform.position.z;

        x = Mathf.Clamp(x, xMin, xMax);
        y = Mathf.Clamp(y, yMin, yMax);
        z = Mathf.Clamp(z, zMin, zMax);
        Vector3 camPos = new Vector3(x, y, z);
        this.transform.position = camPos;

    }


    public void KeyMovement()
    {


        //This block will pan the camera
        float xMove = Input.GetAxisRaw("Horizontal");
        float yMove = Input.GetAxisRaw("Vertical");

        transform.Translate(xMove * Time.deltaTime * keyFloat, 0, 0);

        transform.position += transform.forward * yMove * keyFloat * Time.deltaTime;

        //This block affects camera zoom
        if(Input.GetButton("ZoomIn"))
        {

            Camera.main.orthographicSize += 5 * Time.deltaTime;

            if(camera.orthographic == false)
            {
                camera.fieldOfView += 1 * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, perspectiveMin, perspectiveMax);
            }

            else
            {
                // Make sure the orthographic size never drops below zero.
                Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, cameraMin, cameraMax);
            }



        }
        if (Input.GetButton("ZoomOut"))
        {
            Camera.main.orthographicSize -= 5 * Time.deltaTime;


            if (camera.orthographic == false)
            {
                camera.fieldOfView -= 1 * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, perspectiveMin, perspectiveMax);
            }

            else
            {
                // Make sure the orthographic size never drops below zero.
                Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, cameraMin, cameraMax);

            }



            // Make sure the orthographic size never drops below zero.
            //  Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, cameraMin, cameraMax);
        }


    }

    public void TouchMovement()
    {
        //This block will pan the camera
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

            transform.Translate(-touchDeltaPosition.x * speed, 0, 0);

            transform.position += transform.forward * -touchDeltaPosition.y * 1 * Time.deltaTime;
        }

        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            if (camera.orthographic == true)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

                // Make sure the orthographic size never drops below zero.
                camera.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, cameraMin, cameraMax);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 20, 60);
            }
        }

    }


}
