﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WorldSpaceBillboard : MonoBehaviour {

	// Use this for initialization
	public virtual void Start () {
		
	}
	
	// Update is called once per frame
    
	public virtual void Update ()
    {
        BillboardUIElement();
    }

    public void BillboardUIElement()
    {
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward * 2,
                         Camera.main.transform.rotation * Vector3.up * 2);
    }
}
