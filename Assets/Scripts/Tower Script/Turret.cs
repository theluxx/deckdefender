﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : TowerBase
{
    public GameObject turretHead;


  void Start()
    {
        _lineOfSight = GetComponent<SphereCollider>();

        _lineOfSight.radius = _atkRange * _atkRangeMod;

        playerScript = GameObject.Find("Player").GetComponent<Player>();
        playerScript.towerLived++;

        Initialise();
    }
    // Use this for initialization   

    // Update is called once per frame
    void Update()
    {
        if (_target != null && _target.activeInHierarchy)
        {
            

            turretHead.transform.LookAt(new Vector3(_target.transform.position.x, turretHead.transform.position.y, _target.transform.position.z));
            _atkTimer -= Time.deltaTime;
            if (_atkTimer <= 0.0f)
            {
                if (!_bullet.activeInHierarchy)
                {
                    _bullet.transform.position = _bulletSpawnPoint.transform.position;
                    _bullet.transform.rotation = _bulletSpawnPoint.transform.rotation;
                    _bullet.GetComponent<Bullet>().SetOrigin(_bulletSpawnPoint.transform.position); 
                    _bullet.SetActive(true);


                    _firingSound.Play();
                }

                float enemyHp = _target.GetComponent<EnemyScript>().TakeDamage((int)((float)_baseDmg * _cardDmg));

                EnemyDeathCheck(enemyHp, _target.GetComponent<EnemyScript>());

                _atkTimer = _timerBase;
            }
        }
        else if (_target != null && !_target.activeInHierarchy)
        {
            // Called when enemy is destroyed
            _enemies.Remove(_target);
            _enemies.TrimExcess();

            _target = null;
            // Find next closest enemy
            foreach (GameObject enemy in _enemies)
            {
                if (_target == null)
                {
                    _target = enemy;
                }
                else if (((_target.transform.position - transform.position).magnitude > (enemy.transform.position - transform.position).magnitude))
                {
                    _target = enemy;
                }
            }
        }

    }



}
