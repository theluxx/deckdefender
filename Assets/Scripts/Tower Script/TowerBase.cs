﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Author: Thau Shien Hsu
/// ID: 1007996
/// </summary>



// Change Card type to TowerCard type later

public abstract class TowerBase : MonoBehaviour
//public class TowerBase
{
    [SerializeField] protected Image _hpBar;


    [SerializeField]
    public int imageReference; //This is for a tower menu image
    [SerializeField]
    public TowerNodeButton towerNodeButton;//This is for allowing the tower to revert back to node when destroyed

    [SerializeField] protected int _level = 1;


    [SerializeField] protected GameObject _bullet;
    [SerializeField] protected GameObject _bulletSpawnPoint;


    // HP
    [SerializeField] protected float _currentHp = 20;
    [SerializeField] protected float _baseHp = 20;
    [SerializeField] protected float _cardHp = 1.0f;


    // Damage
    [SerializeField] protected float _baseDmg = 5;
    [SerializeField] protected float _cardDmg = 1.0f;
    

    // Attack Speed
    [SerializeField] protected float _baseSpd = 5;
    [SerializeField] protected float _cardSpd = 1.0f;
    
    // Scrap resource related
    [SerializeField] protected int _scrapCost = 5;
    [SerializeField] protected float _scrapGainMod = 1.0f;
    [SerializeField] protected int _upgradeCost = 10;

    [SerializeField] protected float _upgradeCostMod = 1.0f;

    [SerializeField] protected int _sellGain = 2;
    [SerializeField] protected float _sellGainMod = 1.0f;


    // Score
    [SerializeField] protected int _score;
    [SerializeField] protected float _scoreMod;


    // Attack Range
    [SerializeField] protected float _atkRange = 3.0f;

    [SerializeField] protected float _atkRangeMod = 1.0f;




    [SerializeField] protected string _desc;

    [SerializeField] protected string _abilityDesc;

    [SerializeField] protected TowerCard[] _equippedCards;


    [SerializeField] protected List<GameObject> _enemies;
	[SerializeField] protected GameObject _target;

    [SerializeField] protected SphereCollider _lineOfSight;

    [SerializeField] protected float _atkTimer = 0.0f;
    [SerializeField] protected float _timerBase = 5.0f;

    [SerializeField] protected AudioSource _firingSound;

    [SerializeField] protected GameObject _destroyedExplosion;

    [SerializeField] protected string _effectDesc = "N/A";

    //Player script
    protected Player playerScript;

    public void Initialise()
    {
        RemoveAllCard();

        _hpBar.type = Image.Type.Filled;
        _hpBar.fillMethod = Image.FillMethod.Horizontal;
        _hpBar.fillAmount = 1.0f;

        playerScript = GameObject.Find("Player").GetComponent<Player>();
        _firingSound = GetComponent<AudioSource>();
        _hpBar.color = new Color(0, 255, 0);
    }

    /// <summary>
    /// Called to equip a card to the tower
    /// </summary>
    /// <param name="inCard">The card to be equipped</param>
    /// <param name="slotNum">The slot to equip the card on</param>
    public void EquipCard(TowerCard inCard, int slotNum)
    {
        if (slotNum < _equippedCards.Length)
        {
            _equippedCards[slotNum] = inCard;

            AddCardStats(inCard.cardHp, inCard.cardDmg, inCard.cardSpd, inCard.scrapGainMod, inCard.upgradeCostMod, inCard.scoreMod, inCard.attackRangeMod);

            inCard.OnTowerEquip();
            
        }
    }


    /// <summary>
    /// Removes the specified card
    /// </summary>
    /// <param name="inCard">The card to be removed</param>
    public int RemoveCard(TowerCard inCard)
    {
        int index = 0;

        int i = -1;


        while (index < _equippedCards.Length && i == -1)
        {
            if (_equippedCards[index] == inCard)
            {
                i = index;

                // Leave most of the removal work to RemoveCardInSlot()
                RemoveCardInSlot(i);
            }

            index++;
        }

        return i;
    }

    /// <summary>
    /// Removes the card in the specified slot
    /// </summary>
    /// <param name="slotNum">Slot number.</param>
    public void RemoveCardInSlot(int slotNum)
    {
        TowerCard card = _equippedCards[slotNum];

        RemoveCardStats(card.cardHp, card.cardDmg, card.cardSpd, card.scrapGainMod, card.upgradeCostMod, card.scoreMod, card.attackRangeMod);

        _equippedCards[slotNum] = null;
    }

    public void RemoveAllCard()
    {
        for(int i = 0; i < _equippedCards.Length; i++)
        {
            if (_equippedCards[i] != null)
            {
                RemoveCardInSlot(i);
            }
        }
    }

    public void TributeCard(TowerCard inRemoveCard, TowerCard inNewCard)
    {
        inRemoveCard.OnTowerTribute();
        RemoveCard(inRemoveCard);

        EquipCard(inNewCard, RemoveCard(inRemoveCard));
    }

    /// <summary>
    /// Called when a card is used if there is a need to modify a tower's stats
    /// </summary>
    /// <param name="inCardHp"></param>
    /// <param name="inCardDmg"></param>
    /// <param name="inCardSpd"></param>
    public void AddCardStats(float inCardHp, float inCardDmg, float inCardSpd, float inScrapMod, float inUpgradeMod, float inScoreMod, float inCardRng)
    {
        if (inCardHp != 0)
        {
            _cardHp *= inCardHp;
            _currentHp = (int)((float)_currentHp * inCardHp);
        }

        if (inCardDmg != 0)
        {
            _cardDmg *= inCardDmg;
        }

        if (inCardSpd != 0)
        {
            _cardSpd *= inCardSpd;
        }

		if (inScrapMod != 0)
		{
			_scrapGainMod *= inScrapMod;
		}

        if (inUpgradeMod != 0)
        {
            _upgradeCostMod *= inUpgradeMod;
        }

        if (inScoreMod != 0)
        {
            _scoreMod *= inScoreMod;
        }

        if (inCardRng != 0)
        {
            _atkRange *= inCardRng;
        }

        //		// Scrap resource related
        //		[SerializeField] protected float _scrapGainMod;
        //		[SerializeField] protected int _upgradeCost;
        //
        //		[SerializeField] protected float _upgradeCostMod;
        //
        //		// Score
        //		[SerializeField] protected int _score;
        //		[SerializeField] protected float _scoreMod;
        //
        //
        //		// Attack Range
        //		[SerializeField] protected int _atkRange;
        //
        //		[SerializeField] protected float _atkRangeMod;
    }


    public void RemoveCardStats(float inCardHp, float inCardDmg, float inCardSpd, float inScrapMod, float inUpgradeMod, float inScoreMod, float inCardRng)
    {
        if (inCardHp != 0)
        {
            _cardHp /= inCardHp;

            if (_currentHp > _baseHp * _cardHp)
            {
                _currentHp = (int)((float)_baseHp * inCardHp);
            }
        }

        if (inCardDmg != 0)
        {
            _cardDmg /= inCardDmg;
        }

        if (inCardSpd != 0)
        {
            _cardSpd /= inCardSpd;
        }


        if (inScrapMod != 0)
        {
            _scrapGainMod /= inScrapMod;
        }

        if (inUpgradeMod != 0)
        {
            _upgradeCostMod /= inUpgradeMod;
        }

        if (inScoreMod != 0)
        {
            _scoreMod /= inScoreMod;
        }

        if (inCardRng != 0)
        {
            _atkRange /= inCardRng;
        }
    }


    /// <summary>
    /// Called by enemies to inflict damage to tower
    /// </summary>
    /// <param name="inDamage">In damage.</param>
    public void TakeDamage(float inDamage)
    {

//        if(_currentHp <= 0)
//        {
//            return;
//        }
        _currentHp -= inDamage;

        _hpBar.fillAmount = (float)_currentHp / ((float)_baseHp * _cardHp);

        if (_currentHp <= 0)
        {
            foreach (TowerCard cards in _equippedCards)
            {
                if (cards != null)
                {
                    cards.OnTowerDestroy();
                }

            }

            GameObject explosion = Instantiate(_destroyedExplosion, this.transform.position, Quaternion.identity);
            Destroy(explosion, 3);
            //This restores the node when the tower is destroyed
            
            towerNodeButton.RestoreNodeOnDestroy();



            //Take from the towers lost in player script
            playerScript.towerLost--;

            foreach(GameObject enemy in _enemies)
            {
                EnemyExitRangeEffect(enemy);
            }

            Destroy(this.gameObject);
        }
    }

    /// <summary>
    /// Returns the total scrap to be gained when the tower is sold
    /// </summary>
    /// <returns>Scrap value</returns>
    public int SellTower()
    {
        int value = (int)(_sellGain * _sellGainMod);

        Destroy(gameObject);

        return value;
    }

    /// <summary>
    /// Heals the tower.
    /// </summary>
    /// <param name="inHeal">Heal amount</param>
    public void HealTower(int inHeal)
    {
        _currentHp += inHeal;

        Mathf.Clamp(_currentHp, 0, (int)((float)_baseHp * _cardHp));

        _hpBar.fillAmount = (float)_currentHp / ((float)_baseHp * _cardHp);
    }


	/// <summary>
	/// Register enemies entering range
	/// </summary>
	/// <param name="other">Other.</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            _enemies.Add(other.gameObject);

            EnemyEnterRangeEffect(other.gameObject);

            if (_target == null)
            {
                _target = other.gameObject;

                _atkTimer = _timerBase;
            }
        }
    }


	/// <summary>
	/// Uregistering enemies exiting range
	/// </summary>
	/// <param name="other">Other.</param>
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            EnemyExitRangeEffect(other.gameObject);
            _enemies.Remove(other.gameObject);
            _enemies.TrimExcess();

            if (other.gameObject == _target)
            {
                //_target = null;
                // Find next closest enemy
                foreach (GameObject enemy in _enemies)
                {
                    if (((_target.transform.position - transform.position).magnitude > (enemy.transform.position - transform.position).magnitude) || _target != null)
                    {
                        _target = enemy;
                    }
                }
            }
        }
    }

    public bool LevelUP(int inScrapOnHand, Player player)
    {
        bool lvUp = false;

        if (inScrapOnHand >= (_upgradeCost * _upgradeCostMod))
        {
            int exchange = (int)(_upgradeCost * _upgradeCostMod);
            player.scrap -= exchange;
            float levelUpMultiplier = 1 + (0.8f / (float)_level);

            _baseHp = (int)(_baseHp * levelUpMultiplier);
            _currentHp = (int)(_baseHp * _cardHp);

            _baseDmg = (int)(_baseDmg * levelUpMultiplier);
            _baseSpd =  (int)(_baseSpd * levelUpMultiplier);

            _level++;

            _upgradeCost = (int)(_upgradeCost * ((float)_level * 0.75f));

            lvUp = true;

            

        }

        return lvUp;
    }

    // Get functions
    public int GetLevel()
    {
        return _level;
    }

    public float GetCurrentHP()
    {
        return _currentHp;
    }

    public float GetBaseHP()
    {
        return _baseHp;
    }

    public float GetCardHP()
    {
        return _cardHp;
    }

    public float GetBaseDmg()
    {
        return _baseDmg;
    }

    public float GetCardDmg()
    {
        return _cardDmg;
    }

    public float GetBaseSpd()
    {
        return _baseSpd;
    }

    public float GetCardSpd()
    {
        return _cardSpd;
    }

    public int GetScrapCost()
    {
        return _scrapCost;
    }

    public float GetScrapMod()
    {
        return _scrapGainMod;
    }

    public int GetUpgradeCost()
    {
        return _upgradeCost;
    }

    public float GetUpgradeCostMod()
    {
        return _upgradeCostMod;
    }

    public int GetScore()
    {
        return _score;
    }

    public float GetScoreMod()
    {
        return _scoreMod;
    }

    public float AtkRange()
    {
        return _atkRange;
    }

    public float AtkRangeMod()
    {
        return _atkRangeMod;
    }

    public string GetDesc()
    {
        return _desc;
    }

    public string GetAbilityDesc()
    {
        return _abilityDesc;
    }

    public TowerCard[] GetEquippedCards()
    {
        return _equippedCards;
    }



    public void ActionCardModAtk(float mod, bool modify)
    {
        if(modify == true)
        {
            _cardDmg *= mod;
        }
        else
        {
            _cardDmg /= mod;
            
        }

        
    }


    public void ActionCardModRange(float mod, bool modify)
    {
        if (modify == true)
        {
            _atkRangeMod *= mod;
        }
        else
        {
            _atkRangeMod /= mod;
        }
    }

    public void ActionCardModSpeed(float mod, bool modify)
    {
        if (modify == true)
        {
            _cardSpd *= mod;
        }
        else
        {
            _cardSpd /= mod;
        }
    }





    /// <summary>
    /// This method checks if an enemy is killed by this tower, if it does it calls any equipped cards OnTowerKill() method.
    /// </summary>
    /// <param name="enemyHp"></param>
    protected void EnemyDeathCheck(float enemyHp, EnemyScript enemy)
    {
        if (enemyHp <= 0)
        {
            //calls ontowerkill if the enemies hp is zero or less
            TowerCard[] equippedCards = GetEquippedCards();
            foreach (TowerCard card in equippedCards)
            {


                if (card != null)
                {
                    card.OnTowerKill();
                }
            }

            //reward player with scrap
            playerScript.scrap += (int)(_scrapGainMod * enemy.GetScrapValue());
            playerScript.UpdatePlayerScrap();

        }
    }

    public float ReturnMaxHp()
    {
        return (float)((float)_baseHp * (float)_cardHp);
    }

    public virtual void EnemyEnterRangeEffect(GameObject inEnemy)
    {
    }

    public virtual void EnemyExitRangeEffect(GameObject inEnemy)
    {
    }

}
