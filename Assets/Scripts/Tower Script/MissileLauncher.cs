﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : TowerBase
{
    ///public float _shootTimer = 3.0f;
    public float _recoveryTimer = 0.0f;
    public GameObject _muzzle;
    public GameObject _explosion;
    public AudioClip _firingClip;
    public AudioClip _explosionClip;
    public bool _missileHit = false;

    // Use this for initialization
    void Start()
    {
        _lineOfSight = GetComponent<SphereCollider>();

        _lineOfSight.radius = _atkRange * _atkRangeMod;

        playerScript = GameObject.Find("Player").GetComponent<Player>();
        playerScript.towerLived++;

        Initialise();
    }
	
    // Update is called once per frame
    void Update()
    {
        if (_target != null && _target.activeInHierarchy && _recoveryTimer <= 0.0f)
        {
            if (!_muzzle.activeInHierarchy && _atkTimer >= 0.0f)
            {
                _firingSound.clip = _firingClip;
                _firingSound.Play();
                _muzzle.SetActive(true);

            }


            transform.LookAt(new Vector3(_target.transform.position.x, transform.position.y, _target.transform.position.z));
            _atkTimer -= Time.deltaTime;

            if (_atkTimer <= 0.0f)
            {
                /*
                if (!_bullet.activeInHierarchy)
                {
                    _bullet.transform.position = _bulletSpawnPoint.transform.position;
                    _bullet.transform.rotation = _bulletSpawnPoint.transform.rotation;
                    _bullet.GetComponent<Bullet>().SetOrigin(_bulletSpawnPoint.transform.position); 
                    _bullet.SetActive(true);


                    _firingSound.Play();
                }
                */
                if (_missileHit)
                {
                    float enemyHp = _target.GetComponent<EnemyScript>().TakeDamage((int)((float)_baseDmg * _cardDmg));
                    _muzzle.SetActive(false);
                    _bullet.SetActive(false);
                    _firingSound.clip = _explosionClip;
                    _firingSound.Play();
                    _explosion.SetActive(true);
                    _explosion.transform.position = _target.transform.position;

                    EnemyDeathCheck(enemyHp, _target.GetComponent<EnemyScript>());

                    _recoveryTimer = 1.0f;
                    _missileHit = false;
                    _atkTimer = _timerBase;
                }
                else if(!_bullet.activeInHierarchy)
                {
                    SpawnMissile();
                }
            }
        }
        else if (_target != null && !_target.activeInHierarchy)
        {
            // Called when enemy is destroyed
            _enemies.Remove(_target);
            _enemies.TrimExcess();

            _target = null;
            // Find next closest enemy
            foreach (GameObject enemy in _enemies)
            {
                if (_target == null)
                {
                    _target = enemy;
                }
                else if (((_target.transform.position - transform.position).magnitude > (enemy.transform.position - transform.position).magnitude))
                {
                    _target = enemy;
                }
            }

            if (_target == null)
            {
                _muzzle.SetActive(false);
            }
        }
        else if (_recoveryTimer > 0.0f)
        {
            _recoveryTimer -= Time.deltaTime;

            if (_recoveryTimer <= 0)
            {
                _explosion.SetActive(false);
            }
        }

    }

    public void SpawnMissile()
    {
        Vector3 spawnPoint = new Vector3(_target.transform.position.x, _target.transform.position.y + 10.0f, transform.position.z);
        _bullet.GetComponent<Missile>()._target = _target;
        _bullet.GetComponent<Missile>()._launcher = this;
        _bullet.transform.position = spawnPoint;
        _bullet.SetActive(true);

    }

    public void Hit()
    {
        _missileHit = true;

    }

    public override void EnemyEnterRangeEffect(GameObject inEnemy)
    {
        EnemyScript enemyScript = inEnemy.GetComponent<EnemyScript>();

        if (enemyScript)
        {
            enemyScript.ModifySpeed(0.5f, gameObject);
        }
    }

    public override void EnemyExitRangeEffect(GameObject inEnemy)
    {
        EnemyScript enemyScript = inEnemy.GetComponent<EnemyScript>();

        if (enemyScript)
        {
            enemyScript.ModifySpeed(1.0f, gameObject);
        }
    }
}
