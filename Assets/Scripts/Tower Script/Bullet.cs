﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector3 _origin;

    public float _maxDist = 3.0f;
    public float _spd = 50.0f;

    // Use this for initialization
    void Start()
    {
        gameObject.transform.parent = null;
    }
	
    // Update is called once per frame
    void Update()
    {
        transform.position += (transform.forward * _spd * Time.deltaTime);

        if ((transform.position - _origin).magnitude >= _maxDist)
        {
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider collide)
    {
        gameObject.SetActive(false);
    }

    public void SetOrigin(Vector3 inOrigin)
    {
        _origin = inOrigin;
    }
}
