﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour {
    Player playerScript;

    public int baseHp = 30;
    public Text baseHpText;
    public GameObject explosion;
    [SerializeField]
    private bool hasExploded;

    void Start()
    {
        hasExploded = false;
        playerScript = GameObject.Find("Player").GetComponent<Player>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            playerScript.playerHealth--;
            if(playerScript.playerHealth <= 0)
            {
                playerScript.playerHealth = 0;
            }

            playerScript.livesText.text = playerScript.playerHealth.ToString() + ("/") + playerScript.originHealth.ToString();

            if(playerScript.DeathCheck() == true)
            {
                if(explosion == null)
                {
                    Debug.LogError("No explosion assigned to base");
                    return;
                }

                if(hasExploded == false)
                {
                    GameObject obj = Instantiate(explosion);
                    obj.transform.position = this.transform.position;
                    Destroy(obj, 5f);
                    hasExploded = true;
                }

            }

            other.gameObject.SetActive(false);

            if(playerScript.playerHealth <= 0)
            {
                playerScript.Invoke("GameOver", 1);
            }
        }
    }


}
