﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandZoom : MonoBehaviour {


    [SerializeField]
    private Animator anim;

    [SerializeField]
    public bool enlarged = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnEnlargerClick()
    {
        if(enlarged == false)
        {
            enlarged = true;
            anim.SetTrigger("Enlarge");
        }
        else
        {
            enlarged = false;
            anim.SetTrigger("Shrink");
        }
        
    }

}
