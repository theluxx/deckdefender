﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerNodeButton : MonoBehaviour {

    [SerializeField]
    private Image towerNodeButtonImage;

    [SerializeField]
    private Transform towerNodeCentre;

    [SerializeField]
    public GameObject turretPrefab;

    [SerializeField]
    public List<GameObject> turretPrefabs = new List<GameObject>();

    [SerializeField]
    private HandZoom handZoom;

    [SerializeField]
    private Deck deck;

    [SerializeField]
    public GameObject activeTower;

	// Use this for initialization
	void Start () {

        towerNodeButtonImage = this.gameObject.GetComponent<Image>();
        deck = FindObjectOfType<Deck>();

        handZoom = FindObjectOfType<HandZoom>();

	}
	

    public void OnTowerNodeButtonClick()
    {
        if(handZoom.enlarged == true)
        {
            return;
        }



        deck.towerMenu.gameObject.SetActive(true);
        deck.towerMenu.SetTowerNode(this);


    }

    public void BuildTower()
    {
        activeTower = Instantiate(turretPrefab);
        activeTower.transform.position = towerNodeCentre.position;
        Transform nodeParent = towerNodeCentre.transform.parent.transform.parent.transform;

        

        towerNodeCentre.transform.parent.transform.SetParent(activeTower.transform);

        activeTower.transform.SetParent(nodeParent);

        this.transform.parent.parent.gameObject.SetActive(false);
    }

    public void BuildTower(int towerNumber)
    {
        activeTower = Instantiate(turretPrefabs[towerNumber]);
        activeTower.transform.position = towerNodeCentre.position;
        Transform nodeParent = towerNodeCentre.transform.parent.transform.parent.transform;



        towerNodeCentre.transform.parent.transform.SetParent(activeTower.transform);

        activeTower.transform.SetParent(nodeParent);
        activeTower.GetComponent<TowerBase>().towerNodeButton = this;

        this.transform.parent.parent.gameObject.SetActive(false);
    }

    public void RestoreNodeOnDestroy()
    {
        towerNodeCentre.transform.parent.transform.SetParent(activeTower.transform.parent.transform);
        towerNodeCentre.transform.parent.gameObject.SetActive(true);
        towerNodeCentre.transform.parent.gameObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
        activeTower.SetActive(false);
        if(deck.hand.towerMenu.gameObject.activeInHierarchy && deck.hand.towerMenu.tower == activeTower.GetComponent<TowerBase>())
        {
            deck.hand.towerMenu.CloseAndReset();
        }
    }

}
