﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldSlot : MonoBehaviour {

    [SerializeField]
    private Button actionButton;

    [SerializeField]
    private Hand hand;

    [SerializeField]
    private Deck deck;

    [SerializeField]
    private Sprite mainImage;

    [SerializeField]
    private Animator handAnim;

    [SerializeField]
    public GameObject fieldCard;

    [SerializeField]
    public HandZoom handZoom;

    [SerializeField]
    private GameObject selectedCard;

    public void Awake()
    {
        actionButton = this.gameObject.GetComponent<Button>();
        hand = FindObjectOfType<Hand>();
        deck = FindObjectOfType<Deck>();
        handAnim = hand.gameObject.GetComponent<Animator>();
    }

    // Use this for initialization
    void Start()
    {
        GetComponent<Image>().sprite = mainImage;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        hand.OnFieldClick(fieldCard, hand, this);


    }

    public void RevertImage()
    {
        GetComponent<Image>().sprite = mainImage;
    }


}
