﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CancelButton : MonoBehaviour {


    [SerializeField]
    private Hand hand;


    public void OnCancelClick()
    {
        hand.handAnim.SetTrigger("Shrink");
        hand.handZoom.enlarged = false;
        hand.isSelectingActionCard = false;
        hand.isSelectingFieldCard = false;
        hand.isSelectingTowerCard = false;
        this.gameObject.SetActive(false);
        hand.handZoom.gameObject.GetComponent<Button>().interactable = true;

    }

}
