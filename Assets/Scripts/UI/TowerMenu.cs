﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TowerMenu : MonoBehaviour {

    [SerializeField]
    public TowerBase tower;


    [SerializeField]
    public Button[] cardSlots;

    [SerializeField]
    private GameObject cardMenu;

    [SerializeField]
    private TowerNodeButton towerNode;

    [SerializeField]
    private Button buildButton;

    [SerializeField]
    private Button equipButton;

    [SerializeField]
    private Button upgradeButton;

    [SerializeField]
    private Button sellButton;

    [SerializeField]
    private Hand hand;

    [SerializeField]
    private Player player;

    [SerializeField]
    private Sprite[] towerSprites;

    [SerializeField]
    private Image builtTowerImage;


    //UI Audio files
    [SerializeField]
    public AudioSource upgradeSound;
    [SerializeField]
    public AudioSource destroySound;
    [SerializeField]
    public AudioSource buildSound;
    [SerializeField]
    public AudioSource buttonSound;

    [SerializeField]
    private GameObject towerBuildMenu;

    [SerializeField]
    private GameObject towerStats;


    [SerializeField]
    private TextMeshProUGUI tower1Atk;
    [SerializeField]
    private TextMeshProUGUI tower1Price;
    [SerializeField]
    private TextMeshProUGUI tower1Range;
    [SerializeField]
    private TextMeshProUGUI tower1Speed;
    [SerializeField]
    private TextMeshProUGUI tower2Atk;
    [SerializeField]
    private TextMeshProUGUI tower2Price;
    [SerializeField]
    private TextMeshProUGUI tower2Range;
    [SerializeField]
    private TextMeshProUGUI tower2Speed;
    [SerializeField]
    private TextMeshProUGUI tower3Atk;
    [SerializeField]
    private TextMeshProUGUI tower3Price;
    [SerializeField]
    private TextMeshProUGUI tower3Range;
    [SerializeField]
    private TextMeshProUGUI tower3Speed;

    [SerializeField]
    private TextMeshProUGUI currentAtk;
    [SerializeField]
    private TextMeshProUGUI currentUpgradeCost;
    [SerializeField]
    private TextMeshProUGUI currentRange;
    [SerializeField]
    private TextMeshProUGUI currentSpeed;




    // Use this for initialization
    void Start () {

        Initialize();

        AudioSource[] audios = GetComponents<AudioSource>();
        upgradeSound = audios[0];
        destroySound = audios[1];
        buildSound = audios[2];
        buttonSound = audios[3];

    }

    public void OnEnable()
    {
        Initialize();
    }

    private void Initialize()
    {
        if (towerNode != null)
        {
            if (towerNode.activeTower != null)
            {
                buildButton.interactable = false;
            }
            if (towerNode.activeTower == null)
            {
                equipButton.interactable = false;

            }
        }

        if (tower == null)
        {
            equipButton.interactable = false;
            upgradeButton.interactable = false;
            sellButton.interactable = false;
        }


        else
        {
            buildButton.interactable = false;
            equipButton.interactable = true;
            sellButton.interactable = true;
            towerStats.SetActive(true);
            SetCurrentTowerTexts();
            builtTowerImage.sprite = towerSprites[tower.imageReference];
            TowerCard[] cards = tower.GetEquippedCards();
            for (int i = 0; i < tower.GetEquippedCards().Length; i++)
            {
                if (cards[i] != null)
                {
                    cards[i].gameObject.transform.position = cardSlots[i].transform.position;
                    cards[i].gameObject.transform.SetParent(cardSlots[i].transform);
                    cards[i].gameObject.GetComponent<RectTransform>().localScale = Vector3.one;
                    cards[i].gameObject.GetComponent<RectTransform>().localRotation = Quaternion.Euler(0, 0, 0);
                }

            }
            if (tower.GetLevel() == 3)
            {
                upgradeButton.interactable = false;
            }
            else
            {
                upgradeButton.interactable = true;
            }
        }
        UpdateAvailableSlots();

    }

    public void CloseAndReset()
    {
        Debug.Log("I called close and reset");
        this.gameObject.SetActive(false);
        buildButton.interactable = true;
        if(tower != null)
        {
            TowerCard[] cards = tower.GetEquippedCards();
            for (int i = 0; i < tower.GetEquippedCards().Length; i++)
            {
                if (cards[i] != null)
                {
                    cards[i].gameObject.transform.position = tower.transform.position;
                    cards[i].gameObject.transform.SetParent(tower.transform);
                    cards[i].gameObject.GetComponent<RectTransform>().localScale = Vector3.zero;
                }

            }
        }
        tower = null;
        towerNode = null;
        towerBuildMenu.SetActive(false);

        if (hand.handZoom.enlarged == true)
        {
            hand.transform.SetParent(this.transform.parent);
            hand.handZoom.enlarged = false;
            hand.handAnim.SetTrigger("Shrink");
            hand.handZoom.gameObject.GetComponent<Button>().interactable = true;
        }
        hand.isSelectingTowerCard = false;
        hand.isSelectingActionCard = false;
        hand.isSelectingFieldCard = false;
        towerStats.SetActive(false);

    }


    // Update is called once per frame
    void Update () {
		
	}

    public void EquipButton()
    {
        hand.OnTowerClick();
        hand.transform.SetParent(this.transform);

    }

    private void UpdateAvailableSlots()
    {
        if(tower != null)
        {

            for (int i = 0; i < 3; i++)
            {
                cardSlots[i].interactable = true;
            }
            for (int i = 2; i > (tower.gameObject.GetComponent<TowerBase>().GetLevel() - 1); i--)
            {

                cardSlots[i].interactable = false;
            }
        }
        else
        {
            for (int i = 0; i < cardSlots.Length; i++)
            {
                cardSlots[i].interactable = false;
            }
        }

    }

    public void SetTowerNode(TowerNodeButton node)
    {
        towerNode = node;
    }

    public void BuildTower()
    {
        towerBuildMenu.SetActive(true);
        tower1Price.text = "Cost: " + towerNode.turretPrefabs[0].GetComponent<TowerBase>().GetScrapCost().ToString();
        tower2Price.text = "Cost: " + towerNode.turretPrefabs[1].GetComponent<TowerBase>().GetScrapCost().ToString();
        tower3Price.text = "Cost: " + towerNode.turretPrefabs[2].GetComponent<TowerBase>().GetScrapCost().ToString();
        tower1Atk.text = "Attack: " + towerNode.turretPrefabs[0].GetComponent<TowerBase>().GetBaseDmg().ToString();
        tower2Atk.text = "Attack: " + towerNode.turretPrefabs[1].GetComponent<TowerBase>().GetBaseDmg().ToString();
        tower3Atk.text = "Attack: " + towerNode.turretPrefabs[2].GetComponent<TowerBase>().GetBaseDmg().ToString();
        tower1Range.text = "Range: " + towerNode.turretPrefabs[0].GetComponent<TowerBase>().AtkRange().ToString();
        tower2Range.text = "Range: " + towerNode.turretPrefabs[1].GetComponent<TowerBase>().AtkRange().ToString();
        tower3Range.text = "Range: " + towerNode.turretPrefabs[2].GetComponent<TowerBase>().AtkRange().ToString();
        tower1Speed.text = "Speed: " + towerNode.turretPrefabs[0].GetComponent<TowerBase>().GetBaseSpd().ToString();
        tower2Speed.text = "Speed: " + towerNode.turretPrefabs[1].GetComponent<TowerBase>().GetBaseSpd().ToString();
        tower3Speed.text = "Speed: " + towerNode.turretPrefabs[2].GetComponent<TowerBase>().GetBaseSpd().ToString();



    }

    public void OnSlotClick1()
    {
        hand.OnTowerClick(tower.GetEquippedCards(), 0, 0);
        buttonSound.Play();
    }

    public void OnSlotClick2()
    {
        hand.OnTowerClick(tower.GetEquippedCards(), 1, 1);
        buttonSound.Play();
    }

    public void OnSlotClick3()
    {
        hand.OnTowerClick(tower.GetEquippedCards(), 2, 2);
        buttonSound.Play();
    }

    public void OnBuildTower1()
    {
        towerBuildMenu.SetActive(true);
        if (player.scrap < towerNode.turretPrefabs[0].GetComponent<TowerBase>().GetScrapCost())
        {
            return;
        }

        player.scrap -= towerNode.turretPrefabs[0].GetComponent<TowerBase>().GetScrapCost();
        player.UpdatePlayerScrap();
        towerNode.BuildTower(0);
        buildButton.interactable = false;
        equipButton.interactable = true;
        upgradeButton.interactable = true;
        sellButton.interactable = true;
        tower = towerNode.activeTower.GetComponent<TowerBase>();
        UpdateAvailableSlots();
        buildSound.Play();
        towerBuildMenu.SetActive(false);
        towerStats.SetActive(true);
        builtTowerImage.sprite = towerSprites[tower.imageReference];
        SetCurrentTowerTexts();
    }

    public void OnBuildTower2()
    {
        towerBuildMenu.SetActive(true);
        if (player.scrap < towerNode.turretPrefabs[1].GetComponent<TowerBase>().GetScrapCost())
        {
            return;
        }

        player.scrap -= towerNode.turretPrefabs[1].GetComponent<TowerBase>().GetScrapCost();
        player.UpdatePlayerScrap();
        towerNode.BuildTower(1);
        buildButton.interactable = false;
        equipButton.interactable = true;
        upgradeButton.interactable = true;
        sellButton.interactable = true;
        tower = towerNode.activeTower.GetComponent<TowerBase>();
        UpdateAvailableSlots();
        buildSound.Play();
        towerBuildMenu.SetActive(false);
        towerStats.SetActive(true);
        builtTowerImage.sprite = towerSprites[tower.imageReference];
        SetCurrentTowerTexts();
    }

    public void OnBuildTower3()
    {
        towerBuildMenu.SetActive(true);
        if (player.scrap < towerNode.turretPrefabs[2].GetComponent<TowerBase>().GetScrapCost())
        {
            return;
        }

        player.scrap -= towerNode.turretPrefabs[2].GetComponent<TowerBase>().GetScrapCost();
        player.UpdatePlayerScrap();
        towerNode.BuildTower(2);
        buildButton.interactable = false;
        equipButton.interactable = true;
        upgradeButton.interactable = true;
        sellButton.interactable = true;
        tower = towerNode.activeTower.GetComponent<TowerBase>();
        UpdateAvailableSlots();
        buildSound.Play();
        towerBuildMenu.SetActive(false);
        towerStats.SetActive(true);
        builtTowerImage.sprite = towerSprites[tower.imageReference];
        SetCurrentTowerTexts();
    }


    public void SellTower()
    {
        player.scrap += tower.SellTower();
        player.UpdatePlayerScrap();
        tower.towerNodeButton.RestoreNodeOnDestroy();
        sellButton.interactable = false;
        Debug.Log("I should have sold the tower");

        
    }



    public void UpgradeTower()
    {
        upgradeSound.Play();
        tower.LevelUP(player.scrap, player);
        player.UpdatePlayerScrap();
        SetCurrentTowerTexts();
        UpdateAvailableSlots();
        if(tower.GetLevel() == 3)
        {
            upgradeButton.interactable = false;
        }
    }


    public void SetCurrentTowerTexts()
    {
        currentAtk.text = "Attack: " + (tower.GetBaseDmg() * tower.GetCardDmg()).ToString();
        if(tower.GetLevel() < 3)
        {
            currentUpgradeCost.text = "Upgrade: " + (tower.GetUpgradeCost() * tower.GetUpgradeCostMod());
        }
        else
        {
            currentUpgradeCost.text = "Maxed";
        }
        currentRange.text = "Range: " + (tower.AtkRange() * tower.AtkRangeMod()).ToString();
        currentSpeed.text = "Speed: " + (tower.GetBaseSpd() * tower.GetCardSpd()).ToString();


    }

}
