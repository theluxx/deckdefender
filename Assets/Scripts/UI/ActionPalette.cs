﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPalette : MonoBehaviour {


    [SerializeField]
    public List<ActionSlot> actionSlots = new List<ActionSlot>();

    [SerializeField]
    public HandZoom handZoom;

	// Use this for initialization
	void Start () {
		foreach(ActionSlot slot in actionSlots)
        {
            slot.handZoom = handZoom;                
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
