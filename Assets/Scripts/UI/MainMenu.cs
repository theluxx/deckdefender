﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    //create variables for all UI elements to be turned off and on\

    //Level Select objects
    [SerializeField]
    private GameObject LevelPanel;
    [SerializeField]
    private GameObject LevelOneButton;
    [SerializeField]
    private GameObject LevelTwoButton;
    [SerializeField]
    private GameObject LevelThreeButton;
    [SerializeField]
    private GameObject CloseLevelSelectButton;

    //Base menu items
    private GameObject StartButton;
    private GameObject LevelSelectButton;
    private GameObject StoreButton;


    // Use this for initialization
    void Start()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //Button functiions to call throughout the menus

    //Base Menu Buttons
    //Start Game button, load first level
    public void StartGame()
    {
        SceneManager.LoadScene("Level 1");
    }

    //Level Select Button
    public void LevelSelect()
    {
        //Enable all the items in the level select section of menu 
        LevelPanel.SetActive(true);
        LevelOneButton.SetActive(true);
        LevelTwoButton.SetActive(true);
        LevelThreeButton.SetActive(true);
        CloseLevelSelectButton.SetActive(true);
        
    }

    //Workshop Button, load the workshop containing player cards and decks
    public void Workshop()
    {
        SceneManager.LoadScene("The Workshop");
    }

    //Store button, give the player access to the gem store
    public void Store()
    {
        SceneManager.LoadScene("TheShop");
    }

    //Close the application
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Close Game");
    }


    //Level Select Button Section
    //Load level 1
    public void LoadLevelOne()
    {
        SceneManager.LoadScene("Level 1");
        Debug.Log("Load Level One");
    }

    //Load Level 2
    public void LoadLevelTwo()
    {
        Debug.Log("Load Level Two");
    }

    //load Level 3
    public void LoadLevelThree()
    {
        Debug.Log("Load Level Three");
    }

    //close level select menu
    public void CloseLevelSelect()
    {
        LevelPanel.SetActive(false);
        LevelOneButton.SetActive(false);
        LevelTwoButton.SetActive(false);
        LevelThreeButton.SetActive(false);
        CloseLevelSelectButton.SetActive(false);

        Debug.Log("Close Level Selec");
    }
   }


