﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerButton : MonoBehaviour {

    [SerializeField]
    private TowerBase tower;

    [SerializeField]
    private Image towerButtonImage;

    [SerializeField]
    private Hand hand;

    [SerializeField]
    private HandZoom handZoom;

    [SerializeField]
    private GameObject towerCard;


    public void Start()
    {
        tower = this.gameObject.transform.parent.parent.gameObject.GetComponent<TowerBase>();
        towerButtonImage = this.gameObject.GetComponent<Image>();
        hand = FindObjectOfType<Hand>();
        if(handZoom == null)
        {
            handZoom = FindObjectOfType<HandZoom>();
        }
    }


    public void OnTowerButtonClick()
    {
        Debug.Log("I pressed the tower button");

        if(handZoom.enlarged == true)
        {
            return;
        }
        hand.deck.towerMenu.tower = tower;
        hand.deck.towerMenu.gameObject.SetActive(true);
        


    }

    public Hand ReturnHand()
    {
        return hand;
    }


}
