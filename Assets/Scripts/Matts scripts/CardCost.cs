﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardCost : MonoBehaviour {

    public TextMeshProUGUI Cardcost;

    public int workCostBlueprint;

    public CounterForCardsFinishBluePrints ForCardsFinishBluePrints;

    public GemsForWorkShop workShop;

    public TextMeshProUGUI GemsFor;

    public CardListWorkshopToCollections toCollections;

    public int gems;

    // Use this for initialization
    void Start ()
    {
       
        workCostBlueprint = Random.Range(100, 600);

        Cardcost.text = workCostBlueprint.ToString();

        GemsFor = GameObject.Find("GemTexts").GetComponent<TextMeshProUGUI>();

        workShop = GameObject.Find("GemsShopWorkshop").GetComponent<GemsForWorkShop>();
        toCollections = GameObject.Find("GemsShopWorkshop").GetComponent<CardListWorkshopToCollections>();
        int.TryParse(GemsFor.text, out gems);
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    public void MoneySpent()
    {
        GameObject obj = transform.parent.gameObject;
        GameObject cardobj = transform.parent.parent.gameObject;
        if(ForCardsFinishBluePrints.timeRemaining > 0)
        {
            gems -= workCostBlueprint;
           GemsFor.text =gems.ToString();
            toCollections.CardsForThePlayersCollection.Add(cardobj);

            Destroy(obj);
            Destroy(obj.transform.parent.gameObject);
        }
        else
        {
            Destroy(obj);
            toCollections.CardsForThePlayersCollection.Add(cardobj);
        }
    }
}
