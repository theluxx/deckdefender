﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MoneyManager : MonoBehaviour {

    public int Gems;
    public int _Gems { get; private set; }

    public int GemsForShop;
  
    public TextMeshProUGUI text;
    // Use this for initialization
    void Start ()
    {
        Gems = 0;
        Gems = PlayerPrefs.GetInt("Gems", 0);
        text.text = Gems.ToString();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    
    public void BackButton()
    {
        PlayerPrefs.SetInt("Gems", Gems);
    }

    //add load function to this script

}
