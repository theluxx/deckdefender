﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Sale1Gems : MonoBehaviour {

    public TextMeshProUGUI Name;
    public TextMeshProUGUI Discription;
    public TextMeshProUGUI Saleprice;

    public string Nameofsale;
    public string Discriptionofgemamout;
    public string saleprices;

    public TextMeshProUGUI gemtext;

    public int shopgems;

    //public AudioSource buysound;

    public MoneyManager moneym;

     void Awake()
    {
        
    }



    // Use this for initialization
    void Start ()
    {
        //  buysound = GetComponent<AudioSource>();

        
        SetButton();

        moneym = GameObject.Find("Shop manager").GetComponent<MoneyManager>();
        gemtext = GameObject.Find("GemText").GetComponent<TextMeshProUGUI>();


    }

    private void SetButton()
    {
        Name.text = Nameofsale;
        Discription.text = Discriptionofgemamout;
        Saleprice.text = saleprices;
    }

    public void AddGemsToscreen()
    {
        moneym.Gems += shopgems;
        gemtext.text = moneym.Gems.ToString();
    }

    
}
