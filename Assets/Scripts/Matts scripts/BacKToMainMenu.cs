﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BacKToMainMenu : MonoBehaviour {

    public ShopManager shopManager;
    public MoneyManager moneyManager;

    public DateTime oldDate;

    public int OldDate;

    // Use this for initialization
    void Start ()
    {
        shopManager = GameObject.Find("Shop manager").GetComponent<ShopManager>();
        moneyManager = GameObject.Find("Shop manager").GetComponent<MoneyManager>();

        oldDate = new System.DateTime(1999, 1, 13, 3, 57, 32, 11);

        OldDate = oldDate.Day;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetBackToTheMainMenu()
    {
        SceneManager.LoadScene(1);
        PlayerPrefs.SetInt("Gems", moneyManager.Gems);

        DeckUtils.WriteDeck(shopManager.CardBuyList, "PlayerOwnedBluePrints");

        PlayerPrefs.SetInt("Day", OldDate);
    }

    //add load function to this script
}
