﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Testscript : MonoBehaviour
{
  
    public GameObject scrollViewContent;
    public List<GameObject> TestObject = new List<GameObject>();
    public int spawnNumber;



    

    public Component[] textbutton;

    public MoneyManager mm;

    public ShopManager shopm;

    public PersistentCardDatabase pcd;

    public CardList Week1;
    public CardList Week2;
    public CardList Week3;
    public CardList Week4;
    public CardList Week5;
    public CardList Week6;
    public CardList Week7;

    public List<GameObject> Week1Cards = new List<GameObject>();
    public List<GameObject> Week2Cards = new List<GameObject>();
    public List<GameObject> Week3Cards = new List<GameObject>();
    public List<GameObject> Week4Cards = new List<GameObject>();
    public List<GameObject> Week5Cards = new List<GameObject>();
    public List<GameObject> Week6Cards = new List<GameObject>();
    public List<GameObject> Week7Cards = new List<GameObject>();
    public List<string> cardIDs = new List<string>();

    public DateTime currentDate;
     public DateTime oldDate;

    public int OldDate;
    public int Currentday;



    void Awake()
    {
        foreach (GameObject Week1s in Week1.cardList)
        {
           // cardIDs.Add(Week1s.GetComponent<Card>().cardAsset.GetCardID());
            Week1Cards.Add(Week1s);
        }

        foreach (GameObject Week2s in Week2.cardList)
        {
           // cardIDs.Add(Rarecard.GetComponent<Card>().cardAsset.GetCardID());
            Week2Cards.Add(Week2s);
        }

        foreach (GameObject Week3s in Week3.cardList)
        {
           // cardIDs.Add(Epiccard.GetComponent<Card>().cardAsset.GetCardID());
            Week3Cards.Add(Week3s);
        }

        foreach (GameObject Week4s in Week4.cardList)
        {
          //  cardIDs.Add(Week4s.GetComponent<Card>().cardAsset.GetCardID());
            Week4Cards.Add(Week4s);
        }

        foreach (GameObject Week5s in Week5.cardList)
        {
            cardIDs.Add(Week5s.GetComponent<Card>().cardAsset.GetCardID());
            Week5Cards.Add(Week5s);
        }

        foreach (GameObject Week6s in Week6.cardList)
        {
            cardIDs.Add(Week6s.GetComponent<Card>().cardAsset.GetCardID());
            Week6Cards.Add(Week6s);
        }

        foreach (GameObject Week7s in Week7.cardList)
        {
            cardIDs.Add(Week7s.GetComponent<Card>().cardAsset.GetCardID());
            Week7Cards.Add(Week7s);
        }

        OldDate = 6;


        if (OldDate == 0)
        {

            GameObject buttonBuy = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int i = 0; i < 10; i++)
            {
                TestObject.Add((Week1Cards[i]));
                GameObject go = Instantiate(Week1Cards[i]) as GameObject;
                go.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);


                GameObject buttonsBuy = GameObject.Instantiate(buttonBuy, new Vector3(0, 14.05f, 0), Quaternion.identity);
               

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);

            }
        }
       
        else if ( (OldDate - Currentday) == 7 || (OldDate - Currentday) <13)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int s = 0; s < 10; s++)
            {
                TestObject.Add((Week2Cards[s]));
                GameObject go = Instantiate(Week1Cards[s]) as GameObject;
                go.transform.localScale = new Vector3(1.5f, 1.5f,1.5f);

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);
                

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }

        else if ((OldDate - Currentday) == 14 || (OldDate - Currentday) <=20)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int d = 0; d < 10; d++)
            {
                TestObject.Add((Week3Cards[d]));
                GameObject go = Instantiate(Week3Cards[d]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }

        else if ((OldDate - Currentday) == 21 || (OldDate - Currentday) <= 34)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int f = 0; f < 10; f++)
            {
                TestObject.Add((Week4Cards[f]));
                GameObject go = Instantiate(Week4Cards[f]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }

        else if ((OldDate - Currentday) == 35 || (OldDate - Currentday) <= 41)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int h = 0; h < 10; h++)
            {
                TestObject.Add((Week5Cards[h]));
                GameObject go = Instantiate(Week5Cards[h]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }

        else if ((OldDate - Currentday) == 42 || (OldDate - Currentday) <= 48)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int h = 0; h < 10; h++)
            {
                TestObject.Add((Week6Cards[h]));
                GameObject go = Instantiate(Week6Cards[h]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }
        else if ((OldDate - Currentday) == 49 || (OldDate - Currentday) <= 55)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int h = 0; h < 10; h++)
            {
                TestObject.Add((Week6Cards[h]));
                GameObject go = Instantiate(Week6Cards[h]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }
        else if ((OldDate - Currentday) == 56 || (OldDate - Currentday) <= 62)
        {
            TestObject.Clear();
            GameObject buttonBuys = Resources.Load("Shop And Workshop button perfab/Button") as GameObject;
            for (int h = 0; h < 10; h++)
            {
                TestObject.Add((Week7Cards[h]));
                GameObject go = Instantiate(Week7Cards[h]) as GameObject;

                GameObject buttonsBuy = GameObject.Instantiate(buttonBuys, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }
    }







    // Use this for initialization
    void Start()
    {
        currentDate = new System.DateTime(1999, 1, 13, 3, 57, 32, 11); 
        spawnNumber = 7;
        textbutton = GetComponentsInChildren<Text>();
        mm = GameObject.Find("Shop manager").GetComponent<MoneyManager>();
        shopm = GameObject.Find("Shop manager").GetComponent<ShopManager>();
        pcd = GameObject.Find("PersistentCardDatabase").GetComponent<PersistentCardDatabase>();
        PlayerPrefs.GetInt("Day",OldDate);
        Currentday = currentDate.Day;

      


    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnApplicationQuit()
    {
        int day = currentDate.Day;
        PlayerPrefs.SetInt("Day", day);
    }



}
