﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FromWorkShopToMainScreen : MonoBehaviour {

    public CardListWorkshopToCollections ListWorkshopToCollections;

    public GemsForWorkShop GemsForWork;

	// Use this for initialization
	void Start ()
    {
        ListWorkshopToCollections = GameObject.Find("GemsShopWorkshop").GetComponent<CardListWorkshopToCollections>();
        GemsForWork = GameObject.Find("GemsShopWorkshop").GetComponent<GemsForWorkShop>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void FromWorkShopToMainScreenNow()
    {
        SceneManager.LoadScene(1);

        PlayerPrefs.SetInt("Gems", GemsForWork.GemsForWorkshop);

        DeckUtils.WriteDeck(ListWorkshopToCollections.CardsForThePlayersCollection, "PlayerOwnedCardsFromWorkshop");


    }
}
