﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementNodeScript : MonoBehaviour
{
	public GameObject _nextNode;
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			//Debug.Log("Swap");
			other.GetComponent<EnemyScript>().SetNextNode(_nextNode);
            
		}
	}
}
