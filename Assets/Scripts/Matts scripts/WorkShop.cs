using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class WorkShop : MonoBehaviour
{

    public GameObject scrollViewContent;
    public List<GameObject> BluePrintList = new List<GameObject>();
	public List<GameObject> PlayerOwnedBLuePrints = new List<GameObject> ();
	public List<GameObject> PlayerCardPacks = new List<GameObject> ();
	public List<GameObject> PlayersRewards = new List<GameObject> ();
    public List<GameObject> NewList = new List<GameObject>();
    public List<GameObject> Blank = new List<GameObject>();
 
    public int spawnNumber;

    public int saleAmount;

   // public Component[] textbutton;

    public MoneyManager mm;

    public ShopManager shopm;

    private float timeRemaining;


    public CardListWorkshopToCollections ListWorkshopToCollections;

    



    void Awake()
    {
        DeckUtils.ReadDeck(PlayerOwnedBLuePrints, "PlayerOwnedBluePrints");
        DeckUtils.ReadDeck(PlayerCardPacks, "PlayerCardPacks");
        DeckUtils.ReadDeck(PlayersRewards, "PlayersReward");
        if (File.Exists(Application.persistentDataPath + "/NewList.xml"))
        {
            DeckUtils.ReadDeck(NewList, "NewList");
        }
       
    }

  


    // Use this for initialization
    void Start()
    {
        mm = GameObject.Find("MoneyManager").GetComponent<MoneyManager>();
        shopm = GameObject.Find("MoneyManager").GetComponent<ShopManager>();
        ListWorkshopToCollections = GameObject.Find("GemsShopWorkshop").GetComponent<CardListWorkshopToCollections>();



        foreach (GameObject gameobjectPlayerOwnedBluePrints in PlayerOwnedBLuePrints)
		{
			if (PlayerOwnedBLuePrints == null) {
				return;
			} else {
				BluePrintList.Add (gameobjectPlayerOwnedBluePrints);
			}
				
			
		}

		foreach (GameObject gameobjectPlayercardpacks in PlayerCardPacks) 
		{
			if (PlayerCardPacks == null) {
				return;
			} else {
				BluePrintList.Add (gameobjectPlayercardpacks);
			}

		}

		foreach (GameObject gameobjectPlayerRewards in PlayersRewards) {

			if (PlayersRewards == null) {
				return;
			} else {
				BluePrintList.Add (gameobjectPlayerRewards);
			}

		}

        GameObject buttonTimer = Resources.Load("Shop And Workshop button perfab/TimerButton") as GameObject;

            for (int i = 0; i < BluePrintList.Count; i++)
            {
                GameObject go = Instantiate(BluePrintList[i]) as GameObject;
                go.transform.SetParent(scrollViewContent.transform);
                GameObject buttonsBuy = GameObject.Instantiate(buttonTimer, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, true);
                // textbutton = GetComponentsInChildren<Text>();

            }
       

        if (!File.Exists(Application.persistentDataPath + "/NewList.xml") )
        {
            Debug.Log("File isn't working");
              

            
        }else
        {
            for (int i = 0; i < NewList.Count; i++)
            {

                GameObject go = Instantiate(NewList[i]) as GameObject;
                go.transform.SetParent(scrollViewContent.transform);
                GameObject buttonsBuy = GameObject.Instantiate(buttonTimer, new Vector3(0, 14.05f, 0), Quaternion.identity);

                go.transform.SetParent(scrollViewContent.transform);

                buttonsBuy.transform.SetParent(go.transform, false);
            }
        }
        
        
        
        
     }




    

    // Update is called once per frame
    void Update()
    {
       
    }

    public void CheckCardsOut()
    {
        NewList.Add(scrollViewContent.transform.GetChild(0).gameObject);
        DeckUtils.WriteDeck(NewList,"NewList");
        DeckUtils.WriteDeck(Blank, "PlayerOwnedBluePrints");
        DeckUtils.WriteDeck(Blank, "PlayerCardPacks");
        DeckUtils.WriteDeck(Blank, "PlayersReward");


    }

}


    

