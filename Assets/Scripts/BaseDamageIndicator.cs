﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BaseDamageIndicator : MonoBehaviour
{
    [SerializeField] private AudioClip _scream;
    [SerializeField] private GameObject _explosion;
    [SerializeField] private AudioSource _speaker;
    private float _timer = 2.0f;


    // Use this for initialization
    void Start()
    {
        _speaker = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_timer > 0.0f)
        {
            _timer -= Time.deltaTime;
        }
        else if (_explosion.activeInHierarchy)
        {
            _explosion.SetActive(false);
        }
    }
	
    void OnTriggerEnter(Collider inCollide)
    {
        if (inCollide.gameObject.tag == "Enemy")
        {
            _speaker.PlayOneShot(_scream, 1.0f);
            if (_explosion.activeInHierarchy)
            {
                _explosion.SetActive(false);
            }
            _explosion.SetActive(true);
            _timer = 5.0f;
        }
    }
}
