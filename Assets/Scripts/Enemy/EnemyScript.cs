﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    public Image _hpFill;


    [SerializeField] protected float _hp = 10;
    [SerializeField] protected float _maxHp = 10;
    [SerializeField] protected float _dmg = 2;

    //player is rewarded this much scrap when the enemy is killed
    [SerializeField] protected int scrapValue;

    //This is the initial speed of the navmesh agent, it is reverted to this value when the enemy respawns
    [SerializeField] public float initialSpeed;
    protected float _currentSpeedMod = 1.0f;
    protected List<GameObject> _speedModifiers;


    [SerializeField] protected GameObject _target = null;
    [SerializeField] protected GameObject _nextTarget = null;

    [SerializeField] protected GameObject _nextNode;

    [SerializeField] protected float _atkTimer = 0.75f;
    [SerializeField] protected float _timerBase = 0.75f;

    [SerializeField]
    protected NavMeshAgent agent;

    [SerializeField] protected GameObject _destroyed;


    [SerializeField] protected Particle _firing;


    //Get player script to add research points
    Player playerScript;

    public void Awake()
    {
        initialSpeed = this.gameObject.GetComponent<NavMeshAgent>().speed;
    }


    // Use this for initialization
    public void Initialise()
    {
        if (_speedModifiers == null)
        {
            _speedModifiers = new List<GameObject>();
        }
        _hpFill.type = Image.Type.Filled;
        _hpFill.fillMethod = Image.FillMethod.Horizontal;
        _hpFill.fillAmount = 1;
        _hp = _maxHp;
        _atkTimer = _timerBase;
        _target = null;

        //Revert the speed back to normal
        this.gameObject.GetComponent<NavMeshAgent>().speed = initialSpeed;
        playerScript = GameObject.Find("Player").GetComponent<Player>();
        _hpFill.color = new Color(0f, 255f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
		
    }

    public void Movement()
    {
        transform.LookAt(new Vector3(_nextNode.transform.position.x, transform.position.y, _nextNode.transform.position.z));

        transform.position += transform.forward * 3 * Time.deltaTime;
    }

	public void SetNextNode(GameObject inNextNode)
	{
		_nextNode = inNextNode;
        agent.destination = inNextNode.transform.position;
        Debug.Log("I am being called");
	}

    public float TakeDamage(float inDamage)
    {

        _hp -= inDamage;

        _hpFill.fillAmount = ((float)_hp / (float)_maxHp);

        if (_hp <= 0.0)
        {
            playerScript.killCount++;
            Debug.Log("Kills: " + playerScript.killCount);

            if (_destroyed != null)
            {
                GameObject explosion = Instantiate(_destroyed, this.transform.position, new Quaternion());
                explosion.transform.position = this.transform.position;

                explosion.transform.SetParent(null);
                Destroy(explosion, 1.5f);
            }

            _speedModifiers.Clear();
            GetComponent<NavMeshAgent>().speed = initialSpeed;
            gameObject.SetActive(false);
        }

        //Returns the enemy's remaining hp, used for checking if the enemy is dead
        return _hp;
    }

    /// <summary>
	/// Register towers entering range
	/// </summary>
	/// <param name="other">Other.</param>
    public void SightDetected(GameObject other)
    {
        if (other.tag == "Player")
        {
            if (_target == null)
            {
                _target = other;
                _atkTimer = _timerBase;
            }
            else
            {
                _nextTarget = other;
            }
        }

    }


    /// <summary>
    /// Uregistering enemies exiting range
    /// </summary>
    /// <param name="other">Other.</param>
    public void SightLoss(GameObject other)
    {
        if (other.tag == "Player")
        {
            if (other == _target)
            {
                _target = _nextTarget;

                _nextTarget = null;

            }
            else if (other == _nextTarget)
            {
                _nextTarget = null;
            }
        }
    }

    public void AssignInitialNode(GameObject obj)
    {
        _nextNode = obj;
    }




    public float GetMaxHP()
    {
        return _maxHp;
    }

    public int GetScrapValue()
    {
        return scrapValue;
    }


    /// <summary>
    /// Modifies the speed based on the multiplier input
    /// </summary>
    /// <param name="inSpdMultiplier">In spd multiplier.</param>
    /// <param name="inTower">The tower that is modifying the speed</param>
    public void ModifySpeed(float inSpdMultiplier, GameObject inTower)
    {
        if (inSpdMultiplier < 1.0f)
        {
            _speedModifiers.Add(inTower);

            // use a lower speed
            if (_currentSpeedMod > inSpdMultiplier)
            { 
                _currentSpeedMod = inSpdMultiplier;
                this.gameObject.GetComponent<NavMeshAgent>().speed = initialSpeed * inSpdMultiplier;
                _hpFill.color = new Color(255f, 0f, 255f);
            }
        }
        else
        {
            _speedModifiers.Remove(inTower);
            if (_speedModifiers.Count == 0)
            {

                _currentSpeedMod = 1.0f;
                this.gameObject.GetComponent<NavMeshAgent>().speed = initialSpeed;
                _hpFill.color = new Color(0f, 255f, 0f);
            }
        }
    }


}
