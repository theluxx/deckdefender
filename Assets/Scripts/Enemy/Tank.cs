﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Tank : EnemyScript
{



    // Use this for initialization
    void Start()
    {
        Initialise();
        agent = GetComponent<NavMeshAgent>();
        agent.destination = _nextNode.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
       // Movement();


        if (_target != null)
        {
            _atkTimer -= Time.deltaTime;
            if (_atkTimer <= 0.0f)
            {
                _target.GetComponent<TowerBase>().TakeDamage(_dmg);

                _atkTimer = _timerBase;
            }
        }
    }

    public void OnEnable()
    {
        if(agent != null)
        {
            if(agent.destination == null)
            {
                return;
            }
            agent = GetComponent<NavMeshAgent>();
            agent.destination = _nextNode.transform.position;
        }

    }

}
