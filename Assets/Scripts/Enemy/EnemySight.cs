﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour
{

    public EnemyScript _enemyScript;
    // Use this for initialization
    void Start()
    {
		
    }
	
    // Update is called once per frame
    void Update()
    {
		
    }

    void OnTriggerEnter(Collider other)
    {
        _enemyScript.SightDetected(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        _enemyScript.SightLoss(other.gameObject);
    }
}
