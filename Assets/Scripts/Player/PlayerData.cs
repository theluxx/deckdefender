﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System;

[XmlRoot("SaveData")]
public class PlayerData
{
    //Currencies
    public int dataPoints;
    public int dataGems;
    public int dataScraps;
    public int dataLives;

    void Start()
    {
        try
        {
            Load(GetPath("save.xml"));
        }
        catch (Exception e)
        {
            Debug.Log("Error " + e.Message);
        }
        Save(GetPath("save.xml"));
    }

    //save the file
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(PlayerData));
        var stream = new FileStream(path, FileMode.Create);
        serializer.Serialize(stream, this);
        stream.Close();
    }

    //load the pile
    public PlayerData Load(string path)
    {
        var serializer = new XmlSerializer(typeof(PlayerData));
        var stream = new FileStream(path, FileMode.Open);
        var container = serializer.Deserialize(stream) as PlayerData;
        stream.Close();

        return container;
    }

    //get the paths location
    public string GetPath(string filename)
    {
        return Path.Combine(Application.persistentDataPath, filename);
        //document.Save(Application.persistentDataPath + "/PlayerDeck.xml");
    }
}