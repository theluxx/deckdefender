﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class Player : MonoBehaviour {
    //XML data script reference
    PlayerData playData;

    //Player health 
    [SerializeField]
    public int playerHealth = 10;
    [SerializeField]
    public int originHealth;
    //player prefs extra lives purchasble
    public int extraLives = 0;


    [SerializeField]
    private List<GameObject> dummyList = new List<GameObject>();

    //Currencies
    //Scrap used to build towers and upgrades in game
    [SerializeField]
    public int extraScrap;
    [SerializeField]
    public int scrap = 150;

    //Gems used to buy card(s) pack(s) and avoid blueprint time gates
    //[SerializeField]
   // public int gems;

    //Research points used to purchase card blueprints
    [SerializeField]
    public int researchPoints = 0;

    //Different variables to add to research point after each round
    //Number of enemies killed
    public int killCount;

    //Number of towers survived
    public int towerLived;

    //Number of towers lost 
    public int towerLost;

    //Number of lives left
    public int livesLeft;

    //UI Elements for player
    //Lives
    public Text livesText;
    //Scrap
    public Text scrapText;
    //Gems
    //public Text gemsText;
    //Research Points
    public Text rpText;
    //file 

    //Final screen UI
    public GameObject finalCanvas;
    public GameObject background;
    public GameObject nextButton;
    public GameObject restartButton;
    public Text rewardText;
    public Text endText;
    public GameObject winImage;
    public GameObject loseImage;

    public GameObject pauseCanvas;
    

    //roll number
    public int roll;


    //cards
    [SerializeField]
    public List<GameObject> weeklyList;
    public CardList weekList;
    public CardList PlayersCollection;
    public GameObject rewardAnchor;
    public GameObject reward;
    public List<GameObject> PlayersCollections = new List<GameObject>();
    public List<GameObject> RewardCard = new List<GameObject>();


    //points
    public Text textPoints;
    public Text textName;


	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        
        finalCanvas = GameObject.Find("EndGameCanvas");
        pauseCanvas = GameObject.Find("PauseCanvas");
        background = GameObject.Find("Background");
        restartButton = GameObject.Find("RestartButton");
        nextButton = GameObject.Find("NextButton");
        finalCanvas.SetActive(false);
        pauseCanvas.SetActive(false);

        //refence to data script + load file
        playData = new PlayerData();
        try
        {
            playData = playData.Load(playData.GetPath("save.xml"));
        }
        catch(System.Exception e)
        {
            Debug.Log("" + e.Message);
        }

        playData.Save(playData.GetPath("save.xml"));

        //scrap
        //scrap = scrap + playData.dataScraps;

        //Set extra player lives in player prefs
        extraLives = playData.dataLives;
        //Set player lives to default + what they have purchased
        playerHealth = 10 + extraLives;
        originHealth = playerHealth;
        livesText.text = playerHealth.ToString() + ("/") + originHealth.ToString();

        //gemsText.text = playData.dataGems.ToString();
        scrapText.text = scrap.ToString();


        //weekly cards
        foreach (GameObject card in weekList.cardList)
        {
            weeklyList.Add(card);
        }

	}
	
	// Update is called once per frame
	void Update () {
      if(Input.GetKeyDown(KeyCode.Y))
        {
            Victory();
        }

      if(Input.GetKeyDown(KeyCode.F))
        {
            GameOver();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            DeckUtils.WriteDeck(dummyList, "PlayerOwnedBluePrints");
        }
    }

    //call function when player loses, reaching 0 health 
    public void RoundLoss()
    {
        //Pop up menu to show the round is lost

    }

    //call function at end of round to add up each objective the player has done to reach their research points for the end of the round
    public void AddResearchPoints()
    {
        researchPoints = killCount + (towerLived * 3) + playerHealth;
        playData.dataPoints += researchPoints;
        playData.Save(playData.GetPath("Save.xml"));
    }

    public void UpdatePlayerScrap()
    {
        scrapText.text = scrap.ToString();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void UnpauseButton()
    {
        Time.timeScale = 1;
        pauseCanvas.SetActive(false);
    }

    public void PauseButton()
    {
        PauseGame();
        pauseCanvas.SetActive(true);
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
    }


    /*Function to end the game, when the player dies pull of end game canvas
    Give the player a random reward from a list of 10 different cards or reward points 
    prefabs, cards, week 1
     */
    public void Victory()
    {
        FindObjectOfType<MusicPlayer>().PlayVictory();
        finalCanvas.SetActive(true);
        restartButton.SetActive(false);
        rewardAnchor = GameObject.Find("RewardAnchor");
        roll = Random.Range(0, 9);
        Debug.Log("You rolled: " + roll);
        PauseGame();
        endText.text = ("Victory!");
        winImage.SetActive(true);

        //50% chance of getting points or cards
        //points
        if (roll <= 4)
        {
            roll = Random.Range(50, 200);
            Debug.Log("You Won" +  roll + " Points");
            textPoints.text = roll.ToString();
            textName.text = ("Research Points!");
            playData.dataPoints += roll;
            playData.Save(playData.GetPath("Save.xml"));
        }
        
        //cards
        else
        {
            Debug.Log("You Won X Card");
            roll = Random.Range(0, 9);
            reward = weeklyList[roll];
            Instantiate(reward, rewardAnchor.transform);

            DeckUtils.ReadDeck(PlayersCollections, "PlayerOwnedBluePrints");
            RewardCard.Add(reward);
            List<string> cardIds = new List<string>();
            foreach(GameObject card in PlayersCollections)
            {
                cardIds.Add(card.GetComponent<Card>().cardAsset.GetCardID());
            }

            string query = reward.GetComponent<Card>().cardAsset.GetCardID();

            string result = cardIds.Find(item => item == query);
            if(result != null)
            {
                Debug.Log("This reward already exist in the player's collection");

            }
            else
            {
                Debug.Log("This reward doesn't exist in the player's collection, I should give them this card");
                PlayersCollections.Add(reward);
                DeckUtils.WriteDeck(PlayersCollections, "PlayerOwnedBluePrints");
            }

            //foreach (GameObject inPlayersCollection in PLayersCollection)
            //{
            //    if(inPlayersCollection == reward)
            //    {
            //        RewardCard.Clear();
            //        if(reward.transform.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
            //        {
            //            playData.dataPoints += 100;
            //            playData.Save(playData.GetPath("Save.xml"));
            //        }
            //        else if(reward.transform.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
            //        {
            //            playData.dataPoints += 200;
            //            playData.Save(playData.GetPath("Save.xml"));
            //        }
            //        else if(reward.transform.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Epic)
            //        {
            //            playData.dataPoints += 300;
            //            playData.Save(playData.GetPath("Save.xml"));
            //        }
                    
            //}
            //}

            //if(RewardCard != null)
            //{
            //    DeckUtils.WriteDeck(RewardCard,"PlayersReward");
            //}
            //else { return; }



        }
    }

    public void GameOver()
    {
        FindObjectOfType<MusicPlayer>().PlayDefeat();
        finalCanvas.SetActive(true);
        restartButton.SetActive(true);
        rewardText.text = ("");
        endText.text = ("Defeat!");
        loseImage.SetActive(true);
        textPoints.text = ("Your Base was Destroyed");
        PauseGame();

    }

    public void rewardsPoints()
    {

    }

    public void rewardCard()
    {

    }

    
    
    public bool DeathCheck()
    {
        if(playerHealth <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
