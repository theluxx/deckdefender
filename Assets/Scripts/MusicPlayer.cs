﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    [SerializeField]
    private AudioClip music;

    [SerializeField]
    private AudioClip victory;

    [SerializeField]
    private AudioClip defeat;

    [SerializeField]
    private AudioSource musicPlayer;

	// Use this for initialization
	void Start () {
        musicPlayer = GetComponent<AudioSource>();
        PlayBGM();
	}
	


    public void PlayBGM()
    {
        if(musicPlayer.isPlaying)
        {
            musicPlayer.Stop();
        }
        musicPlayer.clip = music;
        musicPlayer.Play();
        musicPlayer.loop = true;
    }

    public void PlayVictory()
    {
        if (musicPlayer.isPlaying)
        {
            musicPlayer.Stop();
        }
        musicPlayer.clip = victory;
        musicPlayer.Play();
        musicPlayer.loop = false;
    }

    public void PlayDefeat()
    {
        if (musicPlayer.isPlaying)
        {
            musicPlayer.Stop();
        }
        musicPlayer.clip = defeat;
        musicPlayer.Play();
        musicPlayer.loop = false;
    }

}
