﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCapture : MonoBehaviour {


    [SerializeField]
    private int num;

    [SerializeField]
    private string capture;
	// Use this for initialization
	void Start () {

        capture = "capture_" + num.ToString() + ".png";
    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.H))
        {
            UnityEngine.ScreenCapture.CaptureScreenshot(capture);
            Debug.Log("Screenshot taken, name is " + capture.ToString() +".png");
            num++;
            capture = "capture_" + num.ToString() + ".png";
        }
		
	}
}
