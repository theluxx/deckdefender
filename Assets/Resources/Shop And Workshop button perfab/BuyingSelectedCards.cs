﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuyingSelectedCards : MonoBehaviour {

    public TextMeshProUGUI CardCost;

    public int CostofCard;

    public MoneyManager mm;

    public ShopManager shopManager;

    public Testscript testscript;

    // public Text GemText;
    public TextMeshProUGUI Gemtextsec;

    //public Card Card;

    



    void Awake()
    {
        Gemtextsec = GameObject.Find("GemText").GetComponent<TextMeshProUGUI>();
        testscript = GameObject.Find("Content").GetComponent<Testscript>();
        mm = GameObject.Find("Shop manager").GetComponent<MoneyManager>();
        shopManager = GameObject.Find("Shop manager").GetComponent<ShopManager>();
       


        

       

        
    }

    // Use this for initialization
    void Start ()
    {
        if (gameObject.transform.parent.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
        {
            CostofCard = 1000;
            CardCost.text = CostofCard.ToString();
        }
        if (gameObject.transform.parent.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
        {
            CostofCard = 3000;
            CardCost.text = CostofCard.ToString();
        }
        if (gameObject.transform.parent.GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Epic)
        {
            CostofCard = 5000;
            CardCost.text = CostofCard.ToString();
        }







    }
	
	// Update is called once per frame
	void Update ()
    {
        Gemtextsec.text = mm.Gems.ToString();
	}

    public void BuyMyCard()
    {
        GameObject objCard = transform.parent.gameObject;
        if (mm.Gems > CostofCard)
        {
            Debug.Log("IS I WORKINF");
            mm.Gems -= CostofCard;
            shopManager.CardBuyList.Add(objCard);
          
        }
            
       
    }
}
