﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class GetCardsFromPacks : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    public List<GameObject> GetCardsOut = new List<GameObject>();
    public DragForCardPack dragForCard;
    public GameObject SpawnStuff;
    public OtherCardTypes otherCard;
    public GemsForWorkShop GemsForWork;
    public int ResearchPoints;
    public TextMeshProUGUI researchPoints;
    public int index = 0;

    public AudioSource audio;



    public void OnDrop(PointerEventData eventData)
    {
        DragForCardPack dragForCardPack = eventData.pointerDrag.GetComponent<DragForCardPack>();

        otherCard = GameObject.Find("OtherCardTypes").GetComponent<OtherCardTypes>();

        GemsForWork = GameObject.Find("Gems").GetComponent<GemsForWorkShop>();

        audio.GetComponent<AudioSource>();

        if (dragForCardPack != null)
        {
            dragForCardPack.parentToReturnToo = this.gameObject.transform.GetChild(0);
            dragForCard = GameObject.Find("Card Pack(Clone)").GetComponent<DragForCardPack>();


            foreach (var GameobjectsinGetCardsout in dragForCard.CardsinPack)
            {
                GameObject go = Instantiate(GameobjectsinGetCardsout);
                go.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
                go.transform.SetParent(SpawnStuff.transform);
                
            }

            

            Destroy(GameObject.Find("Card Pack(Clone)"));
            audio.Play();



            foreach (var Gameobjects in dragForCard.CardsinPack)
            {
                GetCardsOut.Add(Gameobjects);
            }


            if (GetCardsOut.Contains(dragForCard.CardsinPack[0]))
            {
                if (dragForCard.CardsinPack[0].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                {
                    ResearchPoints += 1500;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(0);
                }
                else if (dragForCard.CardsinPack[0].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 3000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(0);
                }
                else if (dragForCard.CardsinPack[0].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 5000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(0);
                }

            }
            else if (GetCardsOut.Contains(dragForCard.CardsinPack[1]))
            {
                if (dragForCard.CardsinPack[1].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                {
                    ResearchPoints += 1500;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(1);
                }
                else if (dragForCard.CardsinPack[1].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 3000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(1);
                }
                else if (dragForCard.CardsinPack[1].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 5000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(1);
                }

            }
            else if (GetCardsOut.Contains(dragForCard.CardsinPack[2]))
            {
                if (dragForCard.CardsinPack[2].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                {
                    ResearchPoints += 1500;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(2);
                }
                else if (dragForCard.CardsinPack[2].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 3000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(2);
                }
                else if (dragForCard.CardsinPack[2].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                {
                    ResearchPoints += 5000;
                    researchPoints.text = ResearchPoints.ToString();
                    dragForCard.CardsinPack.RemoveAt(2);
                }
                else if (GetCardsOut.Contains(dragForCard.CardsinPack[3]))
                {
                    if (dragForCard.CardsinPack[3].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                    {
                        ResearchPoints += 1500;
                        researchPoints.text = ResearchPoints.ToString();
                        dragForCard.CardsinPack.RemoveAt(3);
                    }
                    else if (dragForCard.CardsinPack[3].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                    {
                        ResearchPoints += 3000;
                        researchPoints.text = ResearchPoints.ToString();
                        dragForCard.CardsinPack.RemoveAt(3);
                    }
                    else if (dragForCard.CardsinPack[3].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                    {
                        ResearchPoints += 5000;
                        researchPoints.text = ResearchPoints.ToString();
                        dragForCard.CardsinPack.RemoveAt(3);
                    }
                    else if (GetCardsOut.Contains(dragForCard.CardsinPack[4]))
                    {
                        if (dragForCard.CardsinPack[4].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                        {
                            ResearchPoints += 1500;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(4);
                        }
                        else if (dragForCard.CardsinPack[4].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                        {
                            ResearchPoints += 3000;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(4);
                        }
                        else if (dragForCard.CardsinPack[4].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                        {
                            ResearchPoints += 5000;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(4);
                        }
                    }
                    else if (GetCardsOut.Contains(dragForCard.CardsinPack[5]))
                    {
                        if (dragForCard.CardsinPack[5].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Normal)
                        {
                            ResearchPoints += 1500;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(5);
                        }
                        else if (dragForCard.CardsinPack[5].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                        {
                            ResearchPoints += 3000;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(5);
                        }
                        else if (dragForCard.CardsinPack[5].GetComponent<Card>().cardAsset.rarity == CardAsset.Rarity.Rare)
                        {
                            ResearchPoints += 5000;
                            researchPoints.text = ResearchPoints.ToString();
                            dragForCard.CardsinPack.RemoveAt(5);
                        }
                    }
                }
            }
        }
    }

                            
                        
                    
                
            
        
    
                    



                            // foreach (object item in FirstList)
                            // {
                            //     if (SecondList.Contains(item))
                            //     {
                            //           SecondList.Remove(item);
                            //       }
                            //       if (ThirdList.Contains(item))
                            //       {
                            //           ThirdList.Remove(item);
                            //      }
                        
    







        


           
            




        
          

          

        
    

    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
