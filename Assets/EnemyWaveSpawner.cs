﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class EnemyPool
{
    public GameObject enemyType;
   // [HideInInspector]
    public List<GameObject> enemies = new List<GameObject>();
}

[System.Serializable]
public class EnemyWaves
{
    public List<int> enemyWave = new List<int>();
}

public class EnemyWaveSpawner : MonoBehaviour {
    [SerializeField]
    private EnemyPool[] pool;

    [SerializeField]
    private GameObject objectToPool;
    private GameObject[] enemiesToPool;

    [SerializeField]
    private int minimumAmount;

    [SerializeField]
    private float delayBetweenSpawn;

    [SerializeField]
    private List<GameObject> pooledObjects;

    [SerializeField]
    public bool willGrow;

    [SerializeField]
    private int spawnAmount;

    [SerializeField]
    private int spawnWaves;

    [SerializeField]
    private int currentWave;


    [SerializeField]
    private float delayBetweenWaves;

    [SerializeField]
    private GameObject firstNode;

    private bool gameEnd = false;
    Player playerScript;

    [SerializeField]
    private GameObject spawnButton;

    [SerializeField]
    public List<EnemyWaves> wave = new List<EnemyWaves>();

    public bool gameOver = false;




    // Use this for initialization
    void Start () {
        playerScript = GameObject.Find("Player").GetComponent<Player>();
        currentWave = 1;

        Vector3 sourcePostion = this.transform.position;
        NavMeshHit closestHit;
        if (NavMesh.SamplePosition(sourcePostion, out closestHit, 500, 1))
        {
            Debug.Log("I am reaching this part");
            sourcePostion = closestHit.position;
            // enemy.AddComponent<NavMeshAgent>();
            //TODO
        }
        else
        {
            Debug.Log("...");
        }

        if (minimumAmount == 0)
        {
            minimumAmount = 5;
        }


        //Pooled objects based off of struct
        foreach (EnemyPool ePool in pool)
        {

            for (int i = 0; i < minimumAmount; i++)
            {
                GameObject enemy = Instantiate(ePool.enemyType,sourcePostion,Quaternion.identity);
                enemy.transform.position = sourcePostion;
                ePool.enemies.Add(enemy);
                enemy.transform.SetParent(this.transform);





                enemy.GetComponent<EnemyScript>().AssignInitialNode(firstNode);
                enemy.SetActive(false);
                

            }


        }



        if (spawnAmount == 0)
        {
            spawnAmount = 3;
        }

        if(spawnWaves == 0)
        {
            spawnWaves = 3;
        }

        if(delayBetweenWaves == 0)
        {
            delayBetweenWaves = 5;
        }

        if(delayBetweenSpawn == 0)
        {
            delayBetweenSpawn = 1;
        }


	}
	
	// Update is called once per frame
	void Update () {
      
        if (currentWave >= spawnWaves)
        {
            if ((GameObject.FindWithTag("Enemy") == null) && (gameEnd == true))
            {
                gameEnd = false;
                Debug.Log("Towers Survived: " + playerScript.towerLived);
                Debug.Log("Total Enemies Killed: " + playerScript.killCount);
                Debug.Log("Lives Left: " + playerScript.playerHealth);

                playerScript.AddResearchPoints();
                Debug.Log("Total Research Points Earned: " + playerScript.researchPoints);
            }
        }

        if ((gameOver == true) && (GameObject.FindWithTag("Enemy") == null) && (gameEnd == true )){
            gameEnd = false;
            playerScript.Victory();
        } 
	}

    public void StartGame()
    {
        StartCoroutine("CommenceWaves", 0);
        Debug.Log("Spawning Waves");
    }

    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }

        if(willGrow)
        {
            GameObject obj = Instantiate(objectToPool);
            obj.transform.position = this.transform.position;
            pooledObjects.Add(obj);
            return obj;
        }

        return null;

    }


    public GameObject GetPooledObject(int spawn)
    {
        for (int i = 0; i < pool[spawn].enemies.Count; i++)
        {
            if (!pool[spawn].enemies[i].activeInHierarchy)
            {
                return pool[spawn].enemies[i];
            }
        }

        if (willGrow)
        {
            GameObject obj = Instantiate(objectToPool);
            obj.transform.position = this.transform.position;
            pooledObjects.Add(obj);
            return obj;
        }

        return null;

    }


    public void CommenceWaves()
    {
        StartCoroutine(SpawnWave());
        spawnButton.SetActive(false);
    }


    /*Spawn enemy waves and force spawn enemy waves
     * Call an individual wave in the ienumerator SpawnWave("Insert what list you want to spawn")
     * Make an array of enemies using integers 0, 1, 2 etc.
     * List will have integeres in it integers will refer to what object to pull from the array/
    */


    public IEnumerator SpawnWave()
    {
        for (int i = 0; i < wave.Count; i++)
        {
            for (int j = 0; j < wave[i].enemyWave.Count; j++)
            {
                GameObject obj = GetPooledObject(wave[i].enemyWave[j]);
                obj.transform.position = this.transform.position;
                obj.transform.rotation = this.transform.rotation;
                obj.GetComponent<EnemyScript>().Initialise();
                obj.GetComponent<EnemyScript>().AssignInitialNode(firstNode);
                obj.SetActive(true);
                yield return new WaitForSeconds(delayBetweenSpawn);
            }

            yield return new WaitForSeconds(delayBetweenWaves);



        }
        Debug.Log("all waves finished");
        gameOver = true;
        gameEnd = true;
    }
}
