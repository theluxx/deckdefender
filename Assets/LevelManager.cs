﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public Button Level01Button, Level02Button, Level03Button;
    int levelPassed;

	// Use this for initialization
	void Start ()
    {
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        Level02Button.interactable = true;
        Level03Button.interactable = true;

        switch (levelPassed)
        {
        case 1:
        Level02Button.interactable = true;
        break;
        case 2:
        Level02Button.interactable = true;
        Level03Button.interactable = true;
        break;
        }

		
	}
	
	public void LevelToLoad(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void ResetPlayerPrefs()
    {
        Level02Button.interactable = false;
        Level03Button.interactable = false;
		levelPassed = 1;
    }
}
