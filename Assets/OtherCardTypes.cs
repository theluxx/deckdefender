﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherCardTypes : MonoBehaviour {


    [SerializeField]
    public CardList normals;
    [SerializeField]
    public CardList RareCards;
    [SerializeField]
    public CardList EpicCards;


    public List<GameObject> NormalCards = new List<GameObject>();

    public List<GameObject> RareCardList = new List<GameObject>();

    public List<GameObject> EpicCardsList = new List<GameObject>();

   
    public List<string> cardIDs = new List<string>();



    private void Awake()
    {
        foreach (GameObject Normalcard in normals.cardList)
        {
            cardIDs.Add(Normalcard.GetComponent<Card>().cardAsset.GetCardID());
            NormalCards.Add(Normalcard);
        }

        foreach (GameObject Rarecard in RareCards.cardList)
        {
            cardIDs.Add(Rarecard.GetComponent<Card>().cardAsset.GetCardID());
            RareCardList.Add(Rarecard);
        }

        foreach (GameObject Epiccard in EpicCards.cardList)
        {
            cardIDs.Add(Epiccard.GetComponent<Card>().cardAsset.GetCardID());
            EpicCardsList.Add(Epiccard);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
