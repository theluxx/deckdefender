﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class WriteIfXMl : MonoBehaviour {

	public List<GameObject>blank = new List<GameObject>();

	public CardList DefaultPlayingDeck;

	public List<GameObject> Defultplayingdecklist = new List<GameObject> ();



	void Awake()
	{
		foreach (GameObject gameobjectDefaultplayingdeck in DefaultPlayingDeck.cardList)
		{
			Defultplayingdecklist.Add (gameobjectDefaultplayingdeck);

		}


        //Validate if GameDeck.xml does not exist
        DeckValidate("GameDeck");

		//if (Defultplayingdecklist != null) {
		//	DeckUtils.WriteDeck (Defultplayingdecklist, "GameDeck");
		//} else {
		//	return;
		//}

	}


	// Use this for initialization
	void Start () 
	{
        if (!File.Exists(Application.persistentDataPath + "/PlayersReward.xml"))
        {
            DeckUtils.WriteDeck(blank, "PlayersReward");
        }
        else
        {
            return;
        }

        if (!File.Exists (Application.persistentDataPath + "/PlayerCardPacks.xml")) {
			DeckUtils.WriteDeck (blank, "PlayerCardPack");
		} else {
			return;
		}

        if (!File.Exists(Application.persistentDataPath + "/PlayerOwnedBluePrints.xml"))
        {
            DeckUtils.WriteDeck(blank, "PlayerOwnedBluePrints");
        }
        else
        {
            return;
        }

       
		if (!File.Exists (Application.persistentDataPath + "/PlayerOwnedCardsFromWorkshop.xml")) {
			DeckUtils.WriteDeck (blank, "PlayerOwnedCardsFromWorkshop");
		} else {
			return;
		}
			




	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void DeckValidate(string path)
    {
        if (!File.Exists(Application.persistentDataPath + "/GameDeck" + path + ".xml"))
        {
            PersistentCardDatabase db = FindObjectOfType<PersistentCardDatabase>();
            List<GameObject> defaultDeck = new List<GameObject>();
           // defaultDeck = db.defaultDeck.cardList;
            Debug.Log("Using default deck, writing default deck to " + path + ".xml");
            DeckUtils.WriteDeck(Defultplayingdecklist, path);
        }
        else
        {
            Debug.Log(path + ".xml already exists");
        }
    }
}
