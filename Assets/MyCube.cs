﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MyCube : MonoBehaviour {



    [SerializeField]
    private GameObject myNode;

    [SerializeField]
    private NavMeshAgent agent;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();

        agent.destination = myNode.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
