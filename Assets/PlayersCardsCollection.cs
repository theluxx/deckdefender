using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersCardsCollection : MonoBehaviour {

    

    public List<GameObject> PlayerCollection = new List<GameObject>();

    public GameObject CardCollectionscrollViewContent;

    void Awake()
    {
		DeckUtils.ReadDeck(PlayerCollection, "PlayerOwnedCardsFromWorkshop");
		DeckUtils.ReadDeck(PlayerCollection,"GameDeck");
    }

    // Use this for initialization
    void Start()
    {
       

        for (int i = 0; i < PlayerCollection.Count; i++)
        {
            GameObject go = Instantiate(PlayerCollection[i]) as GameObject;
            
           
            go.AddComponent<CollectionCardDragAndDrop>();
            
            go.AddComponent<CanvasGroup>();
            go.transform.localScale = new Vector3(1f,1f,1f);

            go.transform.SetParent(CardCollectionscrollViewContent.transform);
          

        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayersCollectionOfCards()
    {
        DeckUtils.WriteDeck(PlayerCollection, "PlayerCollections");
    }
}
