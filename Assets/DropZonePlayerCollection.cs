﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class DropZonePlayerCollection : MonoBehaviour,IDropHandler,IPointerEnterHandler,IPointerExitHandler {

	public List<GameObject> CardPlayerObjects = new List<GameObject> ();

	public TextMeshProUGUI AmountOFCards;
    
    public void OnDrop(PointerEventData eventData)
    {
       CollectionCardDragAndDrop dragAndDrop = eventData.pointerDrag.GetComponent<CollectionCardDragAndDrop>();

        if(dragAndDrop != null)
        {
			if (CardPlayerObjects.Count == 30) {
				return;


			} else {
				dragAndDrop.parentToReturnToo = this.gameObject.transform.GetChild (0);
				foreach (Transform child in transform) {
					print ("Foreach loop: " + child);
				}

				int children = transform.childCount;

				for (int i = 0; i < children; ++i) {
					CardPlayerObjects.Add (transform.GetChild (i).gameObject);
				}
			}
            
        }

		AmountOFCards.text = "30/" + CardPlayerObjects.Count;


        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
